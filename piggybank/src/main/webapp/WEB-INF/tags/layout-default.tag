<%@tag description="Default page template for guests" pageEncoding="UTF-8" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/bootstrap-grid.min.css"/>
    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/bootstrap-reboot.min.css"/>
    <link rel="stylesheet" href="${request.contextPath}/resources/customer/css/style.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="${request.contextPath}/resources/customer/js/bootstrap.bundle.min.js"></script>

    <title>Piggy Bank |
        <jsp:invoke fragment="title"/>
    </title>

</head>
<body>

<c:set var="bgColor" value="${isAdmin ? 'bg-blue' : 'bg-green'}"/>

<nav class="navbar navbar-expand-lg navbar-dark ${bgColor}">
    <div class="container">
        <a class="navbar-brand" href="#"><span class="fa fa-piggy-bank"></span> Piggy Bank</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>

<jsp:doBody/>

</body>
</html>

