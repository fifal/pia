<%@ tag description="Side menu for bank codes template" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tc" tagdir="/WEB-INF/tags/admin" %>

<c:set var="currentUri" value="${requestScope['javax.servlet.forward.request_uri']}" scope="page"/>
<c:set var="addUser" value="${s:mvcUrl('UsersCtl#addUser').build()}" scope="page"/>
<c:set var="listUsers" value="${s:mvcUrl('UsersCtl#listUsers').build()}" scope="page"/>

<div class="col-md-3">
    <div class="card">
        <div class="card-header">Možnosti</div>
        <div class="list-group text-small">
            <a href="${addUser}" class="list-group-item ${currentUri == addUser ? 'font-weight-bold' : ''}">
                <span class="fa fa-plus fa-fw"></span> Přidat zákazníka
            </a>
            <span class="my-2"></span>
            <a href="${listUsers}" class="list-group-item ${currentUri == listUsers ? 'font-weight-bold' : ''}">
                <span class="fa fa-users fa-fw"></span> Zákazníci
            </a>
        </div>
    </div>
</div>