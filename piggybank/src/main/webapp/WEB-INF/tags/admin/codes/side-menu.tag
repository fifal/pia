<%@ tag description="Side menu for bank codes template" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tc" tagdir="/WEB-INF/tags/admin" %>

<c:set var="currentUri" value="${requestScope['javax.servlet.forward.request_uri']}" scope="page"/>
<c:set var="addCode" value="${s:mvcUrl('BankCodesCtl#addCode').build()}" scope="page"/>
<c:set var="listCodes" value="${s:mvcUrl('BankCodesCtl#listCodes').build()}" scope="page"/>
<c:set var="listNotActiveCodes" value="${s:mvcUrl('BankCodesCtl#listNotActiveCodes').build()}" scope="page"/>

<div class="col-md-3">
    <div class="card">
        <div class="card-header">Možnosti</div>
        <div class="list-group text-small">
            <a href="${addCode}"
               class="list-group-item ${currentUri == addCode ? 'font-weight-bold' : ''}">
                <span class="fa fa-plus fa-fw"></span> Přidat kód
            </a>
            <span class="my-2"></span>
            <a href="${listCodes}"
               class="list-group-item ${currentUri == listCodes ? 'font-weight-bold' : ''}">
                <span class="fa fa-check fa-fw"></span> Aktivní
            </a>
            <a href="${listNotActiveCodes}"
               class="list-group-item ${currentUri == listNotActiveCodes ? 'font-weight-bold' : ''}">
                <span class="fa fa-times fa-fw"></span> Neaktivní
            </a>
        </div>
    </div>
</div>