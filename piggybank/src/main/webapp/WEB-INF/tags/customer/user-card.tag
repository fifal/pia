<%@ tag description="Card with user account" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:setLocale value="cs_CZ" scope="session"/>

<div class="col-sm-12 col-md-4 col-lg-3 mb-2">
    <div class="card">
        <div class="card-header text-white">
            <span>Účet</span>
        </div>
        <div class="card-body">
            <div class="card-text">
                <h5>${userEntity.getFullName()}</h5>
                <span class="text-small text-black-50">${userAccount.getAccount().getString()}</span>
            </div>
        </div>
        <ul class="list-group list-group-flush">
            <c:if test="${accountUsableBalance == null}">
                <c:set var="accountUsableBalance" value="0"/>
            </c:if>

            <c:choose>
                <c:when test="${accountUsableBalance >= 0}">
                    <c:set var="accountUsableColor" value=""/>
                </c:when>
                <c:when test="${accountUsableBalance < 0}">
                    <c:set var="accountUsableColor" value="text-danger"/>
                </c:when>
            </c:choose>

            <li class="list-group-item">
                <div class="text-small">Disponibilní zůstatek:</div>
                <div><h5 class="text-black-50 ${accountUsableColor} float-right"><fmt:formatNumber value="${accountUsableBalance}" type="currency"/></h5></div>
            </li>

            <c:if test="${accountBalance == null}">
                <c:set var="accountBalance" value="0"/>
            </c:if>

            <c:choose>
                <c:when test="${accountBalance >= 0}">
                    <c:set var="accountColor" value=""/>
                </c:when>
                <c:when test="${accountBalance < 0}">
                    <c:set var="accountColor" value="text-danger"/>
                </c:when>
            </c:choose>

            <li class="list-group-item">
                <div class="text-small">Účetní zůstatek:</div>
                <div><h5 class="${accountColor} float-right"><fmt:formatNumber value="${accountBalance}" type="currency"/></h5></div>
            </li>
        </ul>
        <a href="${s:mvcUrl('TransactionCtl#new').build()}" class="btn btn-primary btn-card">Nová platba</a>
    </div>
</div>
