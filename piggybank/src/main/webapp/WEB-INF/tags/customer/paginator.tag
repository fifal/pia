<%@ tag description="Paginator component" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@attribute name="paginator" %>
<%@attribute name="paginatorUrl" %>

<div class="row mb-auto">
    <div class="col-3">
        <span class="text-small text-black-50">
            Celkem trasakcí: ${paginator.itemCount}
        </span>
    </div>

    <!-- Komponenta stránkovače -->
    <c:if test="${paginator != null && paginator.pageCount > 1}">
        <div class="col-6">
            <nav>
                <ul class="pagination pagination-sm justify-content-center">

                    <li class="page-item <c:if test="${paginator.pageNumber == paginator.startingPage}">disabled</c:if>">
                        <a class="page-link"
                           href="${s:mvcUrl(paginatorUrl).build()}?page=${paginator.startingPage}&limit=${paginator.pageSize}"
                           aria-label="Next">
                            <span aria-hidden="true">&laquo;&laquo;</span>
                            <span class="sr-only">Předchozí</span>
                        </a>
                    </li>

                    <li class="page-item <c:if test="${paginator.pageNumber == paginator.startingPage}">disabled</c:if>">
                        <a class="page-link"
                           href="${s:mvcUrl(paginatorUrl).build()}?page=${paginator.previous().pageNumber}&limit=${paginator.pageSize}"
                           aria-label="Next">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Předchozí</span>
                        </a>
                    </li>

                    <c:forEach begin="${paginator.startingPage}" end="${paginator.endingPage}" var="i">
                        <li class="page-item <c:if test="${paginator.pageNumber eq i}">active</c:if>">
                            <a class="page-link"
                               href="${s:mvcUrl(paginatorUrl).build()}?page=${i}&limit=${paginator.pageSize}">${i}</a>
                        </li>
                    </c:forEach>

                    <li class="page-item <c:if test="${paginator.pageNumber == paginator.endingPage}">disabled</c:if>">
                        <a class="page-link"
                           href="${s:mvcUrl(paginatorUrl).build()}?page=${paginator.next().pageNumber}&limit=${paginator.pageSize}"
                           aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Další</span>
                        </a>
                    </li>

                    <li class="page-item <c:if test="${paginator.pageNumber == paginator.endingPage}">disabled</c:if>">
                        <a class="page-link"
                           href="${s:mvcUrl(paginatorUrl).build()}?page=${paginator.endingPage}&limit=${paginator.pageSize}"
                           aria-label="Next">
                            <span aria-hidden="true">&raquo;&raquo;</span>
                            <span class="sr-only">Poslední</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </c:if>

    <!-- Select s možnostmi limitů pro stránkování -->
    <div class="col-3 <c:if test="${paginator == null || paginator.pageCount == 1}">offset-6</c:if>">
        <div class="input-group float-right">
            <span class="text-small text-black-50 mb-auto mt-auto mr-3">
                Položek na stránku
            </span>
            <select class="form-control form-control-sm" id="paginatorLimit">
                <c:forEach begin="0" end="${paginator.getLimitsLength()-1}" var="i">
                    <option value="${s:mvcUrl(paginatorUrl).build()}?page=1&limit=${paginator.getLimits()[i]}" <c:if test="${paginator.pageSize == paginator.getLimits()[i]}">selected</c:if>>
                            ${paginator.getLimits()[i]}
                    </option>
                </c:forEach>
            </select>
        </div>
    </div>

    <script src="${request.contextPath}/resources/paginator-limit.js"></script>
</div>
