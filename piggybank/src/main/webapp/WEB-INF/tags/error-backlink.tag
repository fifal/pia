<%@ tag description="Component for backlink button" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@attribute name="isAdmin"%>

<c:choose>
    <c:when test="${isAdmin}">
        <c:set var="backlink" value="${s:mvcUrl('AdminDashboardCtl#dashboard').build()}"/>
        <c:set var="backlinkText" value="Přehled administrace"/>
        <c:set var="outlineColor" value="btn-outline-primary"/>
    </c:when>
    <c:otherwise>
        <c:set var="backlink" value="${s:mvcUrl('DashboardCtl#dashboard').build()}"/>
        <c:set var="backlinkText" value="Přehled účtu"/>
        <c:set var="outlineColor" value="btn-outline-success"/>
    </c:otherwise>
</c:choose>


<a class="btn ${outlineColor}" href="${backlink}"><span class="fa fa-arrow-left fa-fw"></span> ${backlinkText}</a>