<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tc" tagdir="/WEB-INF/tags/admin/users" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<t:layout-admin>
    <jsp:attribute name="title">
        <c:set var="currentUri" value="${requestScope['javax.servlet.forward.request_uri']}" scope="request"/>
        <c:set var="addUri" value="${s:mvcUrl('UsersCtl#addUser').build()}" scope="request"/>
        <c:set var="editUri" value="${s:mvcUrl('UsersCtl#editUser').build()}" scope="request"/>

        <c:choose>
            <c:when test="${fn:contains(currentUri, addUri)}">
                <c:set var="action" value="${s:mvcUrl('UsersCtl#addUserSubmit').build()}" scope="request"/>
                <c:set var="title" value="Přidat zákazníka" scope="request"/>
            </c:when>
            <c:when test="${fn:contains(currentUri, editUri)}">
                <c:set var="action" value="${s:mvcUrl('UsersCtl#editUserSubmit').build()}" scope="request"/>
                <c:set var="title" value="Editovat zákazníka" scope="request"/>
            </c:when>
        </c:choose>
        ${title}
    </jsp:attribute>

    <jsp:body>
        <t:flash-messages/>

        <div class="row mb-md-4">
            <tc:side-menu/>

            <%-- Editace informací o zákazníkovi --%>
            <jsp:include page="user-info.jsp"/>
        </div>

        <%-- Pokud editujeme, možnost přidat účty / kartu --%>
        <c:if test="${fn:contains(currentUri, editUri)}">
            <div class="row mb-4">
                <jsp:include page="user-card.jsp"/>
            </div>

            <div class="row">
                <jsp:include page="user-account.jsp"/>
            </div>
        </c:if>
    </jsp:body>

</t:layout-admin>