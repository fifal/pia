<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Accounts --%>
<div class="col-md-5 offset-md-3">
    <div class="card">
        <div class="card-header">Účty</div>
        <div class="card-body">

            <%-- Form for generating new accounts --%>
            <form:form action="${s:mvcUrl('UsersCtl#newAccount').arg(0, user.user.id).build()}" method="post">
                <button class="btn btn-outline-primary btn-sm" type="submit" title="Vygeneruje nový účet">
                    <span class="fa fa-plus fa-fw"></span> Nový účet
                </button>
            </form:form>

            <hr>

            <table class="table table-responsive table-sm table-hover text-small">
                <thead>
                <tr>
                    <th class="col-6" scope="col">Číslo účtu</th>
                    <th class="col-5" scope="col">Zůstatek</th>
                    <th class="col-1 text-right" scope="col">Akce</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${userAccounts}" var="ua">
                    <tr>
                        <td>${ua.account.string}</td>
                        <fmt:setLocale value="cs_CZ" scope="session"/>
                        <td><fmt:formatNumber value="${accountBalance[ua.account.id]}" type = "currency"/></td>
                        <td class="text-right">
                            <a href="${s:mvcUrl('UsersCtl#deleteAccount').arg(0, ua.account.id).build()}"
                               title="Smazat">
                                <span class="fa fa-trash fa-fw a-trash"></span></a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%-- Cards --%>
<div class="col-md-4">
    <div class="card">
        <div class="card-header">Platební karty</div>
        <div class="card-body">

            <%-- Form for generating new cards --%>
            <form:form action="${s:mvcUrl('UsersCtl#newCard').build()}" method="post" modelAttribute="account">
                <div class="input-group">
                    <form:select path="id" cssClass="form-control mr-2">
                        <form:options items="${userAccounts}" itemValue="account.id" itemLabel="account.string"/>
                    </form:select>

                    <input type="hidden" name="userId" id="userId" value="${user.user.id}"/>

                    <button class="btn btn-outline-primary btn-sm" type="submit"
                            title="Vygeneruje novou platební kartu">
                        <span class="fa fa-plus fa-fw"></span> Nová karta
                    </button>
                </div>
            </form:form>

            <hr>

            <table class="table table-responsive table-sm table-hover text-small">
                <thead>
                <tr>
                    <th class="col-5" scope="col">Číslo karty</th>
                    <th class="col-6" scope="col">Číslo účtu</th>
                    <th class="col-1 text-right" scope="col">Akce</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${userCards}" var="uc">
                    <tr>
                        <td>${uc.card.getHiddenCardNumber()}</td>
                        <td>${uc.card.account.getString()}</td>
                        <td class="text-right">
                            <a href="${s:mvcUrl('UsersCtl#deleteCard').arg(0, uc.card.id).build()}"
                               title="Smazat"><span class="fa fa-trash fa-fw a-trash"></span>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
    </div>
</div>

