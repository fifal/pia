<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src='https://www.google.com/recaptcha/api.js'></script>

<div class="col-md-9">
    <div class="card">
        <div class="card-header">${title}</div>
        <div class="card-body">
            <%-- TODO: CAPTCHA INVISIBLE --%>
            <form:form id="userForm" action="${action}" method="post"
                       modelAttribute="user">
                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="user.firstName" for="name"
                                    class="text-small font-weight-bold">Jméno *</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="user.firstName" class="form-control" id="name" type="text"
                                    required="required"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="user.firstName"/></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="user.lastName" for="user.lastName"
                                    class="text-small font-weight-bold">Příjmení *</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="user.lastName" class="form-control" id="user.lastName" type="text"
                                    required="required"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="user.lastName"/></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="user.pin" for="user.pin"
                                    class="text-small font-weight-bold">Rodné číslo *</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="user.pin" class="form-control" id="user.pin" type="text"
                                    required="required"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="user.pin"/></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="street" for="street" class="text-small">Ulice</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="street" class="form-control" id="street" type="text"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="street"/></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="city" for="city" class="text-small">Město</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="city" class="form-control" id="city" type="text"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="city"/></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="zip" for="zip" class="text-small">PSČ</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="zip" class="form-control" id="zip" type="text"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="zip"/></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="user.email" for="user.pin"
                                    class="text-small font-weight-bold">Email *</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="user.email" class="form-control" id="user.email" type="email"
                                    required="required"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="user.email"/></span>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-2 offset-md-1">
                        <form:label path="user.phoneNumber" for="user.pin"
                                    class="text-small">Telefonní číslo</form:label>
                    </div>
                    <div class="col-md-5">
                        <form:input path="user.phoneNumber" class="form-control" id="user.phoneNumber"
                                    type="text"/>
                    </div>
                    <div class="col-md-4">
                        <span class="text-small text-danger"><form:errors path="user.phoneNumber"/></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5 offset-md-3">
                        <c:set var="recaptchaError"><form:errors path="recaptcha" class="text-small text-danger"/></c:set>
                        <form:errors path="recaptcha" class="text-small text-danger"/>
                        <button class="g-recaptcha form-control btn btn-primary"
                                data-sitekey="${captchaSite}"
                                data-callback='onSubmit'>${title}</button>
                    </div>
                </div>
                <form:input path="user.id" id="user.id" type="hidden"/>
                <form:input path="id" id="id" type="hidden"/>
            </form:form>
        </div>
        <div class="card-footer">
            <div class="text-small text-black-50 text-center">Políčka označená * jsou povinná.</div>
        </div>
    </div>
</div>

<script>
    function onSubmit(token) {
        document.getElementById("userForm").submit();
    }

    <c:if test="${not empty recaptchaError}">
        grecaptcha.reset();
    </c:if>
</script>