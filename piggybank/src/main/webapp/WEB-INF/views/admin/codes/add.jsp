<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tc" tagdir="/WEB-INF/tags/admin/codes" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:layout-admin>
    <jsp:attribute name="title">Přidat kód</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>

        <div class="row">
            <tc:side-menu/>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Přidat bankovní kód</div>
                    <div class="card-body">
                        <form:form class="form" action="${s:mvcUrl('BankCodesCtl#addCodeSubmit').build()}" method="post"
                                   modelAttribute="bankCode">
                            <div class="row">
                                <div class="col-md-2 offset-md-2">
                                    <form:label path="code" for="code" class="text-small"><span
                                            class="fa fa-hashtag fa-fw"></span>
                                        Kód</form:label>
                                </div>
                                <div class="col-md-4">
                                    <form:input path="code" class="form-control" id="code" type="text"/>
                                </div>
                                <div class="col-md-4">
                                    <span class="text-small text-danger"><form:errors path="code"/></span>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-2 offset-md-2">
                                    <form:label path="name" for="name" class="text-small"><span
                                            class="fa fa-tag fa-fw"></span> Název
                                        banky</form:label>
                                </div>
                                <div class="col-md-4">
                                    <form:input path="name" class="form-control" id="name" type="text"/>
                                </div>
                                <div class="col-md-4">
                                    <span class="text-small text-danger"><form:errors path="name"/></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 offset-md-4">
                                    <input class="form-control btn btn-primary" type="submit" value="Přidat kód"/>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>

</t:layout-admin>