<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tc" tagdir="/WEB-INF/tags/admin/codes" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<t:layout-admin>
    <jsp:attribute name="title">Bankovní kódy</jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="${request.contextPath}/resources/admin/js/admin-codes-list.js"></script>
    </jsp:attribute>

    <jsp:body>
        <t:flash-messages/>

        <div class="row">
            <tc:side-menu/>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">${active ? 'A' : 'Nea'}ktivní bankovní kódy</div>
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col-12">
                                <a href="${s:mvcUrl('BankCodesCtl#syncCodes').arg(0, active).build()}" class="btn btn-sm btn-outline-primary float-right"><span class="fa fa-sync fa-fw"></span> Synchronizovat kódy</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-sm  table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">Kód</th>
                                        <th scope="col">Banka</th>
                                        <th scope="col" class="text-right">Akce</th>
                                    </tr>
                                    </thead>
                                    <tbody id="list-snippet">

                                    <jsp:include page="list-snippet.jsp"/>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-admin>