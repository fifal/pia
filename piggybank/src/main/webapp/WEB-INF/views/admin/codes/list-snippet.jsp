<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tc" tagdir="/WEB-INF/tags/admin/codes" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:forEach items="${codes}" var="code">
    <tr class="text-small">
        <td class="col-1 font-weight-bold"><fmt:formatNumber type="number" minIntegerDigits="4" pattern="####"
                                          value="${code.code}"/></td>
        <td class="col-11">${code.name}</td>

        <td class="text-right">
            <a href="#"><span class="fa fa-pencil-alt fa-fw" title="Upravit"></span></a>

            <c:choose>
                <c:when test="${active}">
                    <a href="javascript:;" onclick="setActive('${code.code}', ${!active});"
                       title="Deaktivovat">
                        <span class="fa fa-times fa-fw a-trash"></span>
                    </a>
                </c:when>
                <c:when test="${!active}">
                    <a href="javascript:;" onclick="setActive('${code.code}', ${!active});"
                       title="Aktivovat">
                        <span class="fa fa-check fa-fw a-success"></span>
                    </a>
                </c:when>
            </c:choose>
        </td>
    </tr>
</c:forEach>