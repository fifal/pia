<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<t:layout-admin>
    <jsp:attribute name="title">Dashboard</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Přehled administrace
                    </div>
                    <div class="card-body">
                        <p class="text-small">Vítejte na úvodní stránce administrace Piggy Bank a.s.</p>
                        <hr>

                        <div class="row">
                            <div class="col-12">
                                <h5>Statistiky</h5>
                                <p class="text-small">Níže můžete vidět některé zajímavé statistiky.</p>
                            </div>
                        </div>

                        <div class="row text-small">
                            <div class="col-2 offset-1">Počet zákazníků:</div>
                            <div class="col-9">${usersCount}</div>
                        </div>

                        <div class="row text-small">
                            <div class="col-2 offset-1">Provedené transakce:</div>
                            <div class="col-9">${transactionsCount}</div>
                        </div>

                        <div class="row text-small">
                            <div class="col-2 offset-1">Nezpracované příkazy:</div>
                            <div class="col-9">${unprocessedCount}</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-admin>