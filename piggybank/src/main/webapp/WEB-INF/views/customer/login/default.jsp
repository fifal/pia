<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<t:layout-default>
    <jsp:attribute name="title">Přihlášení</jsp:attribute>

    <jsp:body>
        <div class="container container-login">
            <div class="row">
                <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                    <div class="card">
                        <h5 class="card-header"><span class="fa fa-piggy-bank"></span> Piggy Bank</h5>
                        <div class="card-body">

                            <c:set var="loginUri" value="${s:mvcUrl('LoginCtl#login').build()}"/>

                            <form name='loginForm' class="form" action="${loginUri}" method='POST'>
                                <div class="input-group mb-3">
                                    <input class="form-control" id="login" name="login" type="text"
                                           placeholder="12345678"
                                           aria-describedby="user-addon">
                                    <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="fa fa-user fa-fw " id="user-addon"></span>
                            </span>
                                    </div>
                                </div>

                                <div class="input-group mb-4">
                                    <input class="form-control" id="password" name="password" type="password"
                                           placeholder="1234"
                                           aria-describedby="key-addon">
                                    <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="fa fa-key fa-fw " id="key-addon"></span>
                            </span>
                                    </div>
                                </div>

                                <c:if test="${param.error}">
                                    <div align="center">
                                        <p class="text-small text-danger">Nesprávné zákaznické číslo nebo heslo</p>
                                    </div>
                                </c:if>

                                <input type="submit" class="btn btn-success form-control" value="Přihlásit se">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-default>