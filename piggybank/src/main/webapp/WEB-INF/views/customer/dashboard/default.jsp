<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/customer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="cs_CZ" scope="session"/>

<t:layout-customer>
    <jsp:attribute name="title">Přehled účtu</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>
        <div class="row mb-5">
            <!-- Karta s přehledem stavu účtu -->
            <tc:user-card/>

            <!-- Karta s historií transakcí -->
            <div class="col-sm-12 col-md-8 col-lg-9">
                <div class="card">
                    <div class="card-header text-white">
                        <div>Poslední transakce</div>
                    </div>

                    <!-- Každý řádek má tři sloupce: datum, informace a částku -->
                    <div class="card-body">

                        <!-- Tabulka s historii transakcí -->

                        <c:forEach items="${accountTransactions}" var="transaction" varStatus="status">
                            <c:if test="${status.count <= 5}">

                                <c:choose>
                                    <c:when test="${transaction.paymentOrder.account.id != userAccount.getAccount().id}">
                                        <c:set var="fromAccount" value="${transaction.paymentOrder.account.string}"/>
                                        <c:set var="amount" value="${-transaction.paymentOrder.amount}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="fromAccount" value="${transaction.paymentOrder.fullAccountNumber}"/>
                                        <c:set var="amount" value="${transaction.paymentOrder.amount}"/>
                                    </c:otherwise>
                                </c:choose>

                                <div class="row row-transactions">
                                    <div class="col-3 col-sm-2 col-md-2 col-lg-2">
                                        <div class="text-bold text-small text-center">${transaction.date.toLocalDate().dayOfMonth}</div>
                                        <div class="text-bold text-small text-center">${transaction.paymentOrder.dueDateMonthString}</div>
                                    </div>
                                    <div class="col-5 col-sm-7 col-md-7 col-lg-8">
                                        <div class="text-bold"><a
                                                href="${s:mvcUrl("TransactionCtl#transactionDetail").arg(0, transaction.id).build()}">${fromAccount}</a>
                                        </div>
                                        <div class="text-small">${transaction.transactionType.getText(transaction.transactionType)}</div>
                                    </div>

                                    <c:choose>
                                        <c:when test="${amount >= 0}">
                                            <c:set var="transactionColor" value=""/>
                                        </c:when>
                                        <c:when test="${amount < 0}">
                                            <c:set var="transactionColor" value="text-danger"/>
                                        </c:when>
                                    </c:choose>

                                    <div class="col-4 col-sm-3 col-md-3 col-lg-2 text-right text-horizontal-center">
                                        <span class="${transactionColor} text-bold"><fmt:formatNumber value="${amount}" type="currency"/></span>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-12">
                                <div class="text-small text-center">
                                    <a href="${s:mvcUrl('TransactionCtl#history').build()}">Zobrazit všechny transakce</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Přehled karet -->
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3 mb-2">
                <c:if test="${userCards != null}">
                    <c:set var="card" value="${userCards.get(0).card}"/>
                    <div class="card">
                        <div class="card-header">Platební karta</div>
                        <div class="card-body">
                            <div class="card-text">
                                <h5>${userEntity.fullName}</h5>
                                <span class="text-small text-black-50">${card.hiddenCardNumber}</span>
                            </div>
                        </div>
                        <ul class="list-group list-group-flush text-small">
                            <li class="list-group-item">
                                <div>Vydána k účtu:</div>
                                <div class="float-right">${card.account.string}</div>
                            </li>
                            <li class="list-group-item">
                                <div>Platnost do:</div>

                                <c:set var="validToClass" value="text-danger"/>
                                <c:choose>
                                    <c:when test="${card.isValid()}">
                                        <c:set var="validToClass" value="text-success"/>
                                    </c:when>
                                </c:choose>

                                <div class="float-right ${validToClass} text-bold">${card.getValidToMonthYear()}</div>
                            </li>
                        </ul>
                        <button class="btn btn-outline-primary btn-card-small" disabled><span
                                class="fa fa-cog fa-fw"></span></button>
                    </div>
                </c:if>
            </div>
        </div>

    </jsp:body>
</t:layout-customer>