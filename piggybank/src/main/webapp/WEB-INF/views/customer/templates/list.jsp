<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/customer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="cs_CZ" scope="session"/>

<t:layout-customer>
    <jsp:attribute name="title">Šablony plateb</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Přehled šablon</div>
                    <div class="card-body">

                        <!-- Nadpis, popis, rozpis -->
                        <div class="row mb-5">
                            <div class="col-12">
                                <p class="text-small">
                                    Na následující stránce můžete spravovat své šablony pro příkazy k úhradě.
                                    <a href="${s:mvcUrl('TemplatesCtl#addTemplate').build()}"
                                       class="btn btn-sm btn-outline-primary float-right"><span
                                            class="fa fa-plus"></span> Přidat novou šablonu</a>
                                </p>
                            </div>
                        </div>

                        <hr>

                        <!-- Titulky tabulky -->
                        <div class="row mb-3">
                            <div class="col-4"><span class="text-small text-bold">Název</span></div>
                            <div class="col-4 text-center"><span class="text-small text-bold">Popis</span></div>
                            <div class="col-4 text-right"><span class="text-small text-bold">Možnosti</span></div>
                        </div>

                        <hr>

                        <c:forEach items="${templatesList}" var="template">

                            <div class="row mb-3">
                                <div class="col-4 mt-auto mb-auto">
                                    <div class="text-small">${template.paymentOrderTemplate.title}</div>
                                </div>

                                <div class="col-4">
                                    <div class="text-small"><span
                                            class="font-italic">účet: </span><span>${template.paymentOrderTemplate.fullAccountNumber}</span>
                                    </div>
                                    <c:if test="${!template.paymentOrderTemplate.varSymbol.equals(\"\")}">
                                        <div class="text-small"><span
                                                class="font-italic">VS: </span><span>${template.paymentOrderTemplate.varSymbol}</span>
                                        </div>
                                    </c:if>
                                    <c:if test="${!template.paymentOrderTemplate.constSymbol.equals(\"\")}">
                                        <div class="text-small"><span
                                                class="font-italic">KS: </span><span>${template.paymentOrderTemplate.constSymbol}</span>
                                        </div>
                                    </c:if>
                                    <c:if test="${!template.paymentOrderTemplate.specSymbol.equals(\"\")}">
                                        <div class="text-small"><span
                                                class="font-italic">SS: </span><span>${template.paymentOrderTemplate.specSymbol}</span>
                                        </div>
                                    </c:if>

                                    <!-- Oříznutí přílíš dlouhého textu -->

                                    <c:if test="${!template.paymentOrderTemplate.message.equals(\"\")}">
                                        <c:choose>
                                            <c:when test="${fn:length(template.paymentOrderTemplate.message) > 36}">
                                                <c:set var="message">${fn:substring(template.paymentOrderTemplate.message, 0, 36)}...</c:set>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="message">${template.paymentOrderTemplate.message}</c:set>
                                            </c:otherwise>
                                        </c:choose>


                                        <div class="text-small"><span
                                                class="font-italic"
                                                title="${template.paymentOrderTemplate.message}">zpráva: ${message}</span><span></span>
                                        </div>
                                    </c:if>

                                    <div class="text-small"><span class="font-italic">částka: </span><span
                                            class="text-bold"><fmt:formatNumber
                                            value="${template.paymentOrderTemplate.amount}" type="currency"/></span>
                                    </div>
                                </div>

                                <div class="col-4 mt-auto mb-auto text-right">
                                    <a class="btn btn-sm btn-outline-success"
                                       href="${s:mvcUrl('TransactionCtl#newFromTemplate').arg(0, template.paymentOrderTemplate.id).build()}">Použít</a>
                                    <a class="btn btn-sm btn-outline-primary"
                                       href="${s:mvcUrl('TemplatesCtl#editTemplate').arg(0, template.paymentOrderTemplate.id).build()}">Upravit</a>
                                    <a class="btn btn-sm btn-outline-danger"
                                       href="${s:mvcUrl('TemplatesCtl#deleteTemplate').arg(0, template.paymentOrderTemplate.id).build()}">Smazat</a>
                                </div>
                            </div>

                            <hr>

                        </c:forEach>

                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-customer>