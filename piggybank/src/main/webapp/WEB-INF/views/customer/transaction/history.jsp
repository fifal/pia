<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/customer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="cs_CZ" scope="session"/>

<t:layout-customer>
    <jsp:attribute name="title">Detail transakce</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white">
                        <div>Historie transakcí</div>
                    </div>
                    <div class="card-body">
                        <!-- Tabulka s historii transakcí -->

                        <c:forEach items="${accountTransactions}" var="transaction" varStatus="status">

                                <c:choose>
                                    <c:when test="${transaction.paymentOrder.account.id != userAccount.getAccount().id}">
                                        <c:set var="fromAccount" value="${transaction.paymentOrder.account.string}"/>
                                        <c:set var="amount" value="${-transaction.paymentOrder.amount}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="fromAccount" value="${transaction.paymentOrder.fullAccountNumber}"/>
                                        <c:set var="amount" value="${transaction.paymentOrder.amount}"/>
                                    </c:otherwise>
                                </c:choose>

                                <div class="row row-transactions">
                                    <div class="col-3 col-sm-2 col-md-1 col-lg-1">
                                        <div class="text-bold text-small text-center">${transaction.date.toLocalDate().dayOfMonth}</div>
                                        <div class="text-bold text-small text-center">${transaction.paymentOrder.dueDateMonthString}</div>
                                    </div>
                                    <div class="col-5 col-sm-7 col-md-7 col-lg-8">
                                        <div class="text-bold"><a
                                                href="${s:mvcUrl("TransactionCtl#transactionDetail").arg(0, transaction.id).build()}">${fromAccount}</a>
                                        </div>
                                        <div class="text-small">${transaction.transactionType.getText(transaction.transactionType)}</div>
                                    </div>

                                    <c:choose>
                                        <c:when test="${amount >= 0}">
                                            <c:set var="transactionColor" value=""/>
                                        </c:when>
                                        <c:when test="${amount < 0}">
                                            <c:set var="transactionColor" value="text-danger"/>
                                        </c:when>
                                    </c:choose>

                                    <div class="col-4 col-sm-3 col-md-3 col-lg-3 text-right text-horizontal-center">
                                        <span class="${transactionColor} text-bold"><fmt:formatNumber value="${amount}" type="currency"/></span>
                                    </div>
                                </div>
                        </c:forEach>
                        <hr>
                        <tc:paginator/>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-customer>