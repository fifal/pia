<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/customer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:layout-customer>
    <jsp:attribute name="title">Nová platba</jsp:attribute>
    <jsp:attribute name="scripts"><script
            src="${request.contextPath}/resources/customer/js/transaction.js"></script></jsp:attribute>

    <jsp:body>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <t:flash-messages/>
        <div class="row">
            <!-- Karta s přehledem účtu -->
            <tc:user-card/>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-white">
                        <div>Nová platba</div>
                    </div>
                    <div class="card-body">

                        <div class="row mb-4">
                            <div class="col-2 offset-1">
                                <label for="templateSelect"><span class="text-small">Použít šablonu</span></label>
                            </div>
                            <div class="col-8">
                                <select id="templateSelect" class="form-control">
                                    <option value="reset">Žádná šablona</option>
                                    <c:forEach items="${templates}" var="template">
                                        <option value="${template.paymentOrderTemplate.id}" ${selectedOption == template.paymentOrderTemplate.id ? 'selected' : ''}>${template.paymentOrderTemplate.title}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <form:form class="form" action="${s:mvcUrl('TransactionCtl#newSubmit').build()}" method="post"
                                   modelAttribute="paymentOrder" id="form">

                            <!-- Číslo účtu -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="benefAccount" for="benefAccount"
                                                cssClass="text-small font-weight-bold">Číslo účtu *</form:label>
                                </div>
                                <div class="col-md-5">
                                    <form:input path="benefAccount" class="form-control" id="benefAccount" type="text"
                                                placeholder="Zadejte číslo účtu."/>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text" id="banka-addon">
                                            <span class="fa fa-university fa-fw"></span>
                                        </span>
                                        </div>
                                        <form:select path="benefBankCode" id="benefBankCode" class="form-control"
                                                     aria-describedby="banka-addon">
                                            <c:forEach items="${bankCodeList}" var="bc">
                                                <form:option value="${bc.code}">${bc.code} – ${bc.name}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </div>
                                </div>
                            </div>

                            <!-- Chyby pro číslo účtu -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="benefAccount"/></span>
                                    <span class="text-small text-danger"><form:errors path="benefBankCode"/></span>
                                </div>
                            </div>

                            <!-- Částka -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="amount" for="amount"
                                                cssClass="text-small font-weight-bold">Částka *</form:label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <form:input path="amount" class="form-control text-right" id="amount"
                                                    type="text" aria-describedby="mena-addon"/>
                                        <div class="input-group-append">
                                            <select class="form-control" id="mena-addon">
                                                <option>CZK</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Chyba pro částku -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="amount"/></span>
                                </div>
                            </div>

                            <!-- Variabilní symbol -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="varSymbol" for="varSymbol"
                                                cssClass="text-small">Variabilní symbol</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="varSymbol" class="form-control" id="varSymbol" type="text"/>
                                </div>
                            </div>

                            <!-- Chyba pro variabilní symbol -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="varSymbol"/></span>
                                </div>
                            </div>

                            <!-- Konstantní symbol -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="constSymbol" for="constSymbol"
                                                cssClass="text-small">Konstantní symbol</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="constSymbol" class="form-control" id="constSymbol" type="text"/>
                                </div>
                            </div>

                            <!-- Chyba pro konstantní symbol -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="constSymbol"/></span>
                                </div>
                            </div>

                            <!-- Specifický symbol -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="specSymbol" for="specSymbol"
                                                cssClass="text-small">Specifický symbol</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="specSymbol" class="form-control" id="specSymbol" type="text"/>
                                </div>
                            </div>

                            <!-- Chyba pro specifický symbol -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="specSymbol"/></span>
                                </div>
                            </div>

                            <!-- Zpráva pro příjemce -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="message" for="message"
                                                cssClass="text-small">Zpráva příjemce</form:label>
                                </div>
                                <div class="col-md-8">
                                    <form:input path="message" class="form-control" id="message" type="text"/>
                                </div>
                            </div>

                            <!-- Chyba pro zprávu pro příjemce symbol -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="message"/></span>
                                </div>
                            </div>

                            <!-- Datum splatnosti -->
                            <div class="row">
                                <div class="col-md-2 offset-md-1">
                                    <form:label path="dueDate" for="dueDate"
                                                cssClass="text-small font-weight-bold">Datum splatnosti *</form:label>
                                </div>
                                <div class="col-md-8 mb-3">
                                    <form:input path="dueDate" class="form-control" id="dueDate" type="date"/>
                                </div>
                            </div>

                            <!-- Chyba pro datum splatnosti -->
                            <div class="row">
                                <div class="col-md-8 offset-md-3">
                                    <span class="text-small text-danger"><form:errors path="dueDate"/></span>
                                </div>
                            </div>

                            <!-- Odeslání -->
                            <div class="row mb-3">
                                <div class="col-8 offset-3">
                                    <c:set var="recaptchaError"><form:errors path="recaptcha"
                                                                             class="text-small text-danger"/></c:set>
                                    <form:errors path="recaptcha" class="text-small text-danger font-weight-bold"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-11">
                                    <button class="g-recaptcha btn btn-outline-primary btn-lg float-right"
                                            data-sitekey="${captchaSite}"
                                            data-callback='onSubmit'>Odeslat platbu
                                    </button>
                                </div>
                            </div>

                        </form:form>
                    </div>
                    <div class="card-footer">
                        <div class="text-small text-black-50 text-center">Políčka označená * jsou povinná.</div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function onSubmit(token) {
                afterCaptcha();
            }

            <c:if test="${not empty recaptchaError}">
                grecaptcha.reset();
            </c:if>
        </script>


    </jsp:body>
</t:layout-customer>