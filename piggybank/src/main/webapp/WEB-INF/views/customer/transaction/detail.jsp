<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/customer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="cs_CZ" scope="session"/>

<t:layout-customer>
    <jsp:attribute name="title">Detail transakce</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white">
                        <div>Detail transakce</div>
                    </div>
                    <div class="card-body">

                        <c:choose>
                            <c:when test="${transaction.paymentOrder.account.id != userAccount.getAccount().id}">
                                <c:set var="fromAccount" value="${transaction.paymentOrder.account.string}"/>
                                <c:set var="amount" value="${-transaction.paymentOrder.amount}"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="fromAccount" value="${transaction.paymentOrder.fullAccountNumber}"/>
                                <c:set var="amount" value="${transaction.paymentOrder.amount}"/>
                            </c:otherwise>
                        </c:choose>

                        <c:choose>
                            <c:when test="${amount >= 0}">
                                <c:set var="transactionColor" value=""/>
                            </c:when>
                            <c:when test="${amount < 0}">
                                <c:set var="transactionColor" value="text-danger"/>
                            </c:when>
                        </c:choose>

                        <!-- Základní přehled jako v historii transakcí -->
                        <div class="row">
                            <div class="col-md-1">
                                <div class="text-bold text-small text-md-center">${transaction.date.toLocalDate().dayOfMonth}</div>
                                <div class="text-bold text-small text-md-center">${transaction.paymentOrder.dueDateMonthString}</div>
                            </div>
                            <div class="col-md-9">
                                <div class="text-bold">${fromAccount}</div>
                                <div class="text-small">${transaction.transactionType.getText(transaction.transactionType)}</div>
                            </div>
                            <div class="col-md-2 text-md-right text-horizontal-center"><span
                                    class="${transactionColor} text-bold">
                                <fmt:formatNumber value="${amount}" type="currency"/>
                            </span>
                            </div>
                        </div>

                        <!-- Variabilní,... symboly -->
                        <hr>
                        <div class="row text-left text-row-detail">
                            <div class="col-md-4">
                                <span class="fa fa-info-circle fa-fw"></span>
                                <span class="text-small">
                                <span class="text-bold">Variabilní symbol: </span>
                                ${transaction.paymentOrder.varSymbol}
                            </span>
                            </div>
                            <div class="col-md-4">
                                <span class="fa fa-info-circle fa-fw"></span>
                                <span class="text-small">
                                <span class="text-bold">Konstantní symbol: </span>
                                ${transaction.paymentOrder.constSymbol}
                            </span>
                            </div>
                            <div class="col-md-4">
                                <span class="fa fa-info-circle fa-fw"></span>
                                <span class="text-small">
                                <span class="text-bold">Specifický symbol: </span>
                                ${transaction.paymentOrder.specSymbol}
                            </span>
                            </div>
                        </div>

                        <!-- Číslo účtu -->
                        <hr>
                        <div class="row text-left text-row-detail">
                            <div class="col-md-3">
                                <span class="fa fa-info-circle fa-fw"></span>
                                <span class="text-bold text-small">Číslo účtu příjemce</span>
                            </div>
                            <div class="col-md-9">
                                <span class="text-small">${transaction.paymentOrder.benefAccount} / ${transaction.paymentOrder.benefBankCode}</span>
                            </div>
                        </div>

                        <!-- Typ platby -->
                        <hr>
                        <div class="row text-left text-row-detail">
                            <div class="col-md-3">
                                <span class="fa fa-credit-card fa-fw"></span>
                                <span class="text-bold text-small">Typ transakce</span>
                            </div>
                            <div class="col-md-9">
                                <span class="text-small">${transaction.transactionType.getText(transaction.transactionType)}</span>
                            </div>
                        </div>

                        <!-- Datum transakce -->
                        <hr>
                        <div class="row text-left text-row-detail">
                            <div class="col-md-3">
                                <span class="fa fa-check fa-fw"></span>
                                <span class="text-bold text-small">Datum transakce</span>
                            </div>
                            <div class="col-md-9">
                                <span class="text-small"><fmt:formatDate pattern="dd. MM. yyyy" value="${transaction.paymentOrder.processedDate}"/></span>
                            </div>
                        </div>

                        <!-- ID transakce -->
                        <hr>
                        <div class="row text-left text-row-detail">
                            <div class="col-md-3">
                                <span class="fa fa-hashtag fa-fw"></span>
                                <span class="text-bold text-small">ID transakce</span>
                            </div>
                            <div class="col-md-9">
                                <span class="text-small">${transaction.id}</span>
                            </div>
                        </div>

                        <!-- Zpráva pro příjemce -->
                        <hr>
                        <div class="row text-left text-row-detail">
                            <div class="col-md-3">
                                <span class="fa fa-comment fa-fw"></span>
                                <span class="text-bold text-small">Zpráva pro příjemce</span>
                            </div>
                            <div class="col-md-9">
                                <span class="text-small">${transaction.paymentOrder.message}</span>
                            </div>
                        </div>

                        <!-- Poznámka -->
                        <hr>
                        <div class="row text-left text-row-detail">
                            <div class="col-md-3">
                                <span class="fa fa-sticky-note fa-fw"></span>
                                <span class="text-bold text-small">Poznámka</span>
                            </div>
                            <div class="col-md-9">
                                <div>
                                    <textarea class="form-control mb-2"></textarea>
                                    <input type="submit" class="btn btn-primary btn-sm float-right" value="Uložit poznámku">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-customer>