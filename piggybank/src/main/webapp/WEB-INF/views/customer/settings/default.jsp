<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<fmt:setLocale value="cs_CZ" scope="session"/>

<t:layout-customer>
    <jsp:attribute name="title">Nastavení účtu</jsp:attribute>

    <jsp:body>
        <t:flash-messages/>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header text-white">
                        <span>Nastavení účtu</span>
                    </div>
                    <div class="card-body">
                        <div class="card-text">
                            <p class="text-small">Na následující stránce můžete upravit některé ze svých uživatelských
                                údajů.</p>
                            <hr>
                            <form:form modelAttribute="user" action="${s:mvcUrl(\"SettingsCtl#editSubmit\").build()}"
                                       method="post" cssClass="form">

                                <!-- Základní údaje - jméno, příjmení, rodné číslo -->
                                <div class="row">
                                    <div class="offset-1">
                                        <p class="text-small"><span class="fa fa-user"></span> Základní údaje</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1 offset-md-1">
                                        <form:label path="user.firstName" for="user.firstName"
                                                    class="text-small">Jméno</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="user.firstName" class="form-control" id="user.firstName"
                                                    type="text"
                                                    readonly="true"/>
                                    </div>


                                    <div class="col-md-1">
                                        <form:label path="user.lastName" for="user.lastName"
                                                    class="text-small">Příjmení</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="user.lastName" class="form-control" id="user.lastName"
                                                    type="text"
                                                    readonly="true"/>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1 offset-md-1">
                                        <form:label path="user.pin" for="user.pin"
                                                    class="text-small">Rodné číslo</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="user.pin" class="form-control" id="user.pin" type="text"
                                                    readonly="true"/>
                                    </div>
                                </div>

                                <!-- Kontaktní údaje - editovatelné položky -->
                                <div class="row mt-4">
                                    <div class="offset-1">
                                        <p class="text-small"><span class="fa fa-address-card"></span> Kontaktní údaje
                                        </p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1 offset-md-1">
                                        <form:label path="user.email" for="user.email"
                                                    class="text-small">Email</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="user.email" class="form-control" id="user.email" type="email"/>
                                    </div>

                                    <div class="col-md-1">
                                        <form:label path="user.phoneNumber" for="user.phoneNumber"
                                                    class="text-small">Telefon</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="user.phoneNumber" class="form-control" id="user.phoneNumber"
                                                    type="text"/>
                                    </div>
                                </div>

                                <!-- Chybové hlášky pro email a telefonní číslo -->
                                <div class="row">
                                    <div class="col-md-4 offset-md-2">
                                        <span class="text-small text-danger"><form:errors path="user.email"/></span>
                                    </div>
                                    <div class="col-md-4 offset-md-1">
                                        <span class="text-small text-danger"><form:errors
                                                path="user.phoneNumber"/></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1 offset-md-1">
                                        <form:label path="street" for="street" class="text-small">Ulice</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="street" class="form-control" id="street" type="text"/>
                                    </div>

                                    <div class="col-md-1">
                                        <form:label path="city" for="city" class="text-small">Město</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="city" class="form-control" id="city" type="text"/>
                                    </div>
                                </div>

                                <!-- Chybové hlášky pro ulici a město -->
                                <div class="row">
                                    <div class="col-md-4 offset-md-2">
                                        <span class="text-small text-danger"><form:errors path="street"/></span>
                                    </div>
                                    <div class="col-md-4 offset-md-1">
                                        <span class="text-small text-danger"><form:errors
                                                path="city"/></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1 offset-md-1">
                                        <form:label path="zip" for="zip"
                                                    class="text-small">PSČ</form:label>
                                    </div>
                                    <div class="col-md-4">
                                        <form:input path="zip" class="form-control" id="zip" type="text"/>
                                    </div>
                                </div>

                                <!-- Chybová hláška pro PSČ -->
                                <div class="row">
                                    <div class="col-md-4 offset-md-2">
                                        <span class="text-small text-danger"><form:errors path="zip"/></span>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-11">
                                        <input class="btn btn-outline-primary float-right" id="submit" type="submit"
                                               value="Uložit údaje">
                                    </div>
                                </div>

                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-customer>