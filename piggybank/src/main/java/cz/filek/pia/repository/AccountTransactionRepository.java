package cz.filek.pia.repository;

import cz.filek.pia.model.AccountTransaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for account_transactions
 *
 * @author Filip Jani
 */
@Repository
public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, Integer>
{
    /**
     * Finds all transactions by account ID
     *
     * @param accountId int
     * @return  List<AccountTransactions>
     */
    List<AccountTransaction> findAccountTransactionsByAccountId(int accountId);

    /**
     * Finds all transactions limited by pageable
     *
     * @param accountId int
     * @param pageable Pageable
     * @return List<AccountTransactions>
     */
    List<AccountTransaction> findAccountTransactionsByAccountId(int accountId, Pageable pageable);


    Integer countAllByAccountId(int accountId);

    /**
     * Finds account balance by account ID
     *
     * @param accountId int
     * @return Integer
     */
    @Query("SELECT SUM(CASE WHEN p.account.id <> ?1 THEN ABS(p.amount) ELSE p.amount END) as balance FROM AccountTransaction at JOIN PaymentOrder p ON at.paymentOrder.id = p.id where at.account.id = ?1")
    Integer findAccountBalanceByAccountId(int accountId);

    /**
     * Finds transaction by ID
     *
     * @param id : ID of the transaction
     * @return AccountTransaction
     */
    AccountTransaction findById(int id);

    /**
     * Returns count of all account transactions
     *
     * @return Integer
     */
    @Query("SELECT COUNT(at) FROM AccountTransaction at")
    Integer countAll();
}
