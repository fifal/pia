package cz.filek.pia.repository;

import cz.filek.pia.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for user_roles
 *
 * @author Filip Jani
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long>
{
    /**
     * Returns list of user roles for user with userId
     * @param userId : ID of the user
     *
     * @return List<UserRole>
     */
    @Query("SELECT roles FROM UserRole roles WHERE user_id = ?1")
    List<UserRole> findUserRolesByUserId(int userId);
}
