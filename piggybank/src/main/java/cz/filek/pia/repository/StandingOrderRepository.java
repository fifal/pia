package cz.filek.pia.repository;

import cz.filek.pia.model.StandingOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

/**
 * Repository for standing_order
 *
 * @author Filip Jani
 */
@Repository
public interface StandingOrderRepository extends JpaRepository<StandingOrder, Integer>
{
    /**
     * Returns StandingOrder by ID
     *
     * @param id : ID of the standing order
     * @return StandingOrder
     */
    StandingOrder findById(int id);

    /**
     * Finds all standing orders which have start time after startTime and stop time before stopTime
     *
     * @param startTime : LocalDateTime
     * @param stopTime : LocalDateTime
     * @return List<StandingOrder>
     */
    @Query("SELECT so FROM StandingOrder so WHERE so.standingStartDate >= ?1 AND so.standingStopDate <= ?2 AND so.isActive = ?3")
    List<StandingOrder> findAllByDateAndActive(Date startTime, Date stopTime, boolean active);
}
