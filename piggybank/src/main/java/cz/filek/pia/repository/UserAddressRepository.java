package cz.filek.pia.repository;

import cz.filek.pia.model.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 */
@Repository
public interface UserAddressRepository extends JpaRepository<UserAddress, Long>
{
    UserAddress findUserAddressByUserId(int userId);
}
