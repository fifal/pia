package cz.filek.pia.repository;

import cz.filek.pia.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for user_accounts
 *
 * @author Filip Jani
 */
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long>
{
    /**
     * Finds all accounts which belongs to user with userId
     *
     * @param userId int user's ID
     * @return List<UserAccount>
     */
    @Query("SELECT accounts FROM UserAccount accounts WHERE user_id = ?1")
    List<UserAccount> findUserAccountsByUserId(int userId);

    /**
     * Finds user account by account ID
     *
     * @param accountId int account ID
     * @return UserAccount
     */
    UserAccount findUserAccountByAccountId(int accountId);
}
