package cz.filek.pia.repository;

import cz.filek.pia.model.Account;
import cz.filek.pia.model.BankCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for accounts
 *
 * @author Filip Jani
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long>
{
    /**
     * Finds account with given prefix, number and bank code
     *
     * @param prefix String
     * @param number String
     * @param bankCode BankCode
     * @return Account
     */
    Account findByPrefixAndNumberAndBankCode(String prefix, String number, BankCode bankCode);

    /**
     * Finds account with given number and bank code
     *
     * @param number String
     * @param bankCode String
     * @return Account
     */
    Account findByNumberAndBankCode(String number, BankCode bankCode);

    /**
     * Finds account by ID
     *
     * @param id int account ID
     * @return Account
     */
    Account findById(int id);
}
