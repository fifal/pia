package cz.filek.pia.repository;

import cz.filek.pia.model.UserCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for user_cards
 *
 * @author Filip Jani
 */
@Repository
public interface UserCardRepository extends JpaRepository<UserCard, Long>
{
    /**
     * Returns list of cards for user with user_id
     * @param userId : ID of the user
     * @return List<UserCard>
     */
    @Query("SELECT cards FROM UserCard cards WHERE user_id = ?1")
    List<UserCard> findUserCardsByUserId(int userId);

    /**
     * Finds UserCard entity by card ID
     *
     * @param cardId int
     * @return UserCard
     */
    UserCard findUserCardByCardId(int cardId);
}
