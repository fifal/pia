package cz.filek.pia.repository;

import cz.filek.pia.model.User;
import cz.filek.pia.model.UserPaymentTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for user_payment_template
 *
 * @author Filip Jani
 */
@Repository
public interface UserPaymentTemplatesRepository extends JpaRepository<UserPaymentTemplate, Integer>
{
    /**
     * Returns list of UserPaymentTemplates by user
     * @param user : User entity
     * @return List<UserPaymentTemplate>
     */
    List<UserPaymentTemplate> findByUser(User user);

    /**
     * Finds UserPaymentTemplate by ID
     * @param id : ID of the user payment template
     * @return UserPaymentTemplate
     */
    UserPaymentTemplate findById(int id);

    /**
     * Finds UserPaymentTemplate entity by PaymentOrderTemplate ID
     * @param id : ID of the payment order template
     * @return UserPaymentTemplate
     */
    @Query("SELECT upt FROM UserPaymentTemplate upt JOIN PaymentOrderTemplate pot ON upt.paymentOrderTemplate.id = pot.id WHERE pot.id = ?1")
    UserPaymentTemplate findByPaymentOrderTemplateId(int id);
}
