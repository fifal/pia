package cz.filek.pia.repository;

import cz.filek.pia.model.User;
import cz.filek.pia.model.enums.ERole;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for user
 *
 * @author Filip Jani
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>
{
    /**
     * Returns User entity by login
     * @param login : String login of the user
     * @return User
     */
    User findUserByLogin(String login);

    /**
     * Returns User entity by id
     * @param id : ID of the user
     * @return User
     */
    User findById(int id);

    /**
     * Finds all users with role
     *
     * @param role : ERole of the users
     * @param sort : Sort
     * @return List<User>
     */
    @Query("SELECT u FROM User u JOIN UserRole ur ON ur.user.id = u.id JOIN Role r ON r.id = ur.role.id WHERE r.role = ?1")
    List<User> findAllByRole(ERole role, Sort sort);

    /**
     * Counts all users by role
     *
     * @param role : ERole
     * @return Integer
     */
    @Query("SELECT COUNT(u) FROM User u JOIN UserRole ur ON ur.user.id = u.id JOIN Role r ON r.id = ur.role.id WHERE r.role = ?1")
    Integer countAllByRole(ERole role);
}
