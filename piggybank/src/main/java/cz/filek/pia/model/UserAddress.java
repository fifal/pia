package cz.filek.pia.model;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity for user address
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "user_address")
public class UserAddress
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "user_id")
    @NotNull
    @Valid
    private User user;

    @Column(name = "city", length = 50)
    @Size(max = 50, message = "Město musí být v rozmezí {min} - {max} znaků.")
    private String city;

    @Column(name = "street", length = 100)
    @Size(max = 100, message = "Ulice musí být v rozmezí {min} - {max} znaků.")
    private String street;

    @Column(name = "zip")
    @Size(max = 5, message = "PSČ musí mít maximálně {max} znaků.")
    private String zip;

    @Transient
    private boolean recaptcha;

    /**
     * Returns address PK ID
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Setter for id
     * @param id int
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Returns user entity
     * @return User
     */
    public User getUser()
    {
        return user;
    }

    /**
     * Setter for user entity
     * @param user User
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * Returns name of the city
     * @return String
     */
    public String getCity()
    {
        return city;
    }

    /**
     * Setter for city name
     * @param city String
     */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
     * Returns street name with no.
     * @return String
     */
    public String getStreet()
    {
        return street;
    }

    /**
     * Setter for street name
     * @param street String
     */
    public void setStreet(String street)
    {
        this.street = street;
    }

    /**
     * Returns city's ZIP code
     * @return String
     */
    public String getZip()
    {
        return zip;
    }

    /**
     * Setter for city's ZIP
     * @param zip String
     */
    public void setZip(String zip)
    {
        this.zip = zip;
    }

    /**
     * Returns result of recaptcha
     * @return boolean
     */
    public boolean isRecaptcha()
    {
        return recaptcha;
    }

    /**
     * Setter for result of recaptcha
     * @param recaptcha boolean
     */
    public void setRecaptcha(boolean recaptcha)
    {
        this.recaptcha = recaptcha;
    }
}
