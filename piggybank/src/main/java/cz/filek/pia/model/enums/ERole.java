package cz.filek.pia.model.enums;

import java.io.Serializable;

/**
 * Enum for e_role type from DB
 *
 * @author Filip Jani
 */
public enum ERole implements Serializable
{
    admin,
    user;

    @Override
    public String toString()
    {
        switch (this)
        {
            case admin:
                return "admin";
            case user:
            default:
                return "user";
        }
    }
}
