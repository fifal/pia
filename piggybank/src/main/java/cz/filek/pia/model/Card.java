package cz.filek.pia.model;

import javax.persistence.*;
import java.sql.Date;

/**
 * Entity for bank cards
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "card")
public class Card
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "number", length = 16)
    private String number;

    @Column(name = "valid_from")
    private Date validFrom;

    @Column(name = "valid_to")
    private Date validTo;

    @Column(name = "cvv", length = 3)
    private String cvv;

    @Column(name = "is_blocked")
    private boolean isBlocked;

    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;

    /**
     * Returns card id
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Setter for ID
     * @param id int
     */
     public void setId(int id)
     {
     this.id = id;
     }

    /**
     * Returns card number as a string
     * @return String
     */
    public String getNumber()
    {
        return number;
    }

    /**
     * Setter for card number
     * @param number String
     */
    public void setNumber(String number)
    {
        this.number = number;
    }

    /**
     * Returns card's valid from date
     * @return Date
     */
    public Date getValidFrom()
    {
        return validFrom;
    }

    /**
     * Setter for card's valid from date
     * @param validFrom Date
     */
    public void setValidFrom(Date validFrom)
    {
        this.validFrom = validFrom;
    }

    /**
     * Returns card's valid to date
     * @return Date
     */
    public Date getValidTo()
    {
        return validTo;
    }

    /**
     * Setter for card's valid to date
     * @param validTo Date
     */
    public void setValidTo(Date validTo)
    {
        this.validTo = validTo;
    }

    /**
     * Returns card's CVV code
     * @return String
     */
    public String getCvv()
    {
        return cvv;
    }

    /**
     * Setter for card's CVV
     * @param cvv String
     */
    public void setCvv(String cvv)
    {
        this.cvv = cvv;
    }

    /**
     * Returns true if card is blocked
     * @return boolean
     */
    public boolean isBlocked()
    {
        return isBlocked;
    }

    /**
     * Setter for card status
     * @param blocked boolean
     */
    public void setBlocked(boolean blocked)
    {
        isBlocked = blocked;
    }

    /**
     * Returns Account entity
     * @return Account
     */
    public Account getAccount()
    {
        return account;
    }

    /**
     * Setter for Account entity
     * @param account Account
     */
    public void setAccount(Account account)
    {
        this.account = account;
    }

    /**
     * Returns censored card number in format: 4511********1234
     */
    public String getHiddenCardNumber()
    {
        return this.number.substring(0,3) + "********" + this.number.substring(11, number.length() - 1);
    }

    /**
     * Returns if card is valid
     *
     * @return boolean
     */
    public boolean isValid()
    {
        return this.validTo.after(new java.util.Date());
    }

    /**
     * Returns card valid date in formatted String: mm/yy
     *
     * @return String
     */
    public String getValidToMonthYear()
    {
        String year = String.valueOf(this.validTo.toLocalDate().getYear());
        return this.validTo.toLocalDate().getMonth().getValue() + "/" + year.substring(2);
    }
}
