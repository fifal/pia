package cz.filek.pia.model;

import javax.persistence.*;

/**
 * Entity for account
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "account")
public class Account
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "bank_code")
    private BankCode bankCode;

    @Column(name = "number", length = 10, unique = true)
    private String number;

    @Column(name = "prefix", length = 4)
    private String prefix;

    /**
     * Returns PK ID
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Returns bank code
     * @return BankCode
     */
    public BankCode getBankCode()
    {
        return bankCode;
    }

    /**
     * Setter for bank code
     * @param bankCode BankCode
     */
    public void setBankCode(BankCode bankCode)
    {
        this.bankCode = bankCode;
    }

    /**
     * Returns account number
     * @return String
     */
    public String getNumber()
    {
        return number;
    }

    /**
     * Setter for account number
     * @param number String
     */
    public void setNumber(String number)
    {
        this.number = number;
    }

    /**
     * Returns account number prefix
     * @return String
     */
    public String getPrefix()
    {
        return prefix;
    }

    /**
     * Setter for account number prefix
     * @param prefix String
     */
    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }

    /**
     * Returns number of account in format of : prefix-number/bankCode
     * @return
     */
    public String getString()
    {
        return this.prefix == null ? this.number + "/" + this.bankCode.getCode() : this.prefix + "-" + this.number + "/" + this.bankCode.getCode();
    }
}