package cz.filek.pia.model;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Entity for standing_order_log
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "standing_order_log")
public class StandingOrderLog
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "standing_order_id")
    private StandingOrder standingOrder;

    @Column(name = "standing_order_sent_datetime")
    private LocalDateTime sentDateTime;

    /**
     * Returns PK ID
     * @return Integer
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * Setter for ID
     * @param id Integer
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * Returns StandingOrder entity
     * @return StandingOrder
     */
    public StandingOrder getStandingOrder()
    {
        return standingOrder;
    }

    /**
     * Setter for StandingOrder
     * @param standingOrder StandingOrder
     */
    public void setStandingOrder(StandingOrder standingOrder)
    {
        this.standingOrder = standingOrder;
    }

    /**
     * Returns last time order was sent
     * @return LocalDateTime
     */
    public LocalDateTime getSentDateTime()
    {
        return sentDateTime;
    }

    /**
     * Setter for last time sent
     * @param sentDateTime LocalDateTime
     */
    public void setSentDateTime(LocalDateTime sentDateTime)
    {
        this.sentDateTime = sentDateTime;
    }
}
