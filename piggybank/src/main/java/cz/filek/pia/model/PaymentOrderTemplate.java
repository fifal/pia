package cz.filek.pia.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Entity for payment order template
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "payment_order_template")
public class PaymentOrderTemplate extends PaymentOrderBasic implements Serializable
{
    @Column(name = "title", length = 255)
    @Size(max = 255, message = "Musí být v rozmezí {min} - {max} znaků.")
    protected String title;

    /**
     * Returns title of the template
     * @return String
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Setter for the template title
     * @param title String
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
}

