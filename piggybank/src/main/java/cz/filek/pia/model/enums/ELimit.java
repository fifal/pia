package cz.filek.pia.model.enums;

import java.io.Serializable;

/**
 * Enum for card limit types
 *
 * @author Filip Jani
 */
public enum ELimit implements Serializable
{
    internet,
    withdraw,
    pay
}
