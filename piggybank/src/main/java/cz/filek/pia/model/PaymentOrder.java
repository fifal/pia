package cz.filek.pia.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Entity for payment order
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "payment_order")
public class PaymentOrder extends PaymentOrderBasic implements Serializable
{
    @Column(name = "processed_date")
    private Date processedDate;

    @Column(name = "due_date")
    protected Date dueDate;

    @Transient
    private boolean recaptcha;

    /**
     * Default constructor
     */
    public PaymentOrder(){}

    /**
     * Returns PaymentOrder from PaymentOrderTemplate
     * @param paymentOrderTemplate : PaymentOrderTemplate
     */
    public PaymentOrder(PaymentOrderTemplate paymentOrderTemplate)
    {
        this.account = paymentOrderTemplate.account;
        this.amount = paymentOrderTemplate.amount;
        this.benefAccount = paymentOrderTemplate.benefAccount;
        this.benefBankCode = paymentOrderTemplate.benefBankCode;
        this.constSymbol = paymentOrderTemplate.constSymbol;
        this.varSymbol = paymentOrderTemplate.varSymbol;
        this.specSymbol = paymentOrderTemplate.specSymbol;
        this.message = paymentOrderTemplate.message;
        this.dueDate = new Date(new java.util.Date().getTime());
    }

    /**
     * Returns PaymentOrder from StandingOrder
     * @param standingOrder : StandingOrder
     */
    public PaymentOrder(StandingOrder standingOrder)
    {
        this.account = standingOrder.account;
        this.amount = -Math.abs(standingOrder.amount);
        this.benefAccount = standingOrder.benefAccount;
        this.benefBankCode = standingOrder.benefBankCode;
        this.constSymbol = standingOrder.constSymbol;
        this.varSymbol = standingOrder.varSymbol;
        this.specSymbol = standingOrder.specSymbol;
        this.message = standingOrder.message;
        this.dueDate = new Date(new java.util.Date().getTime());
    }

    /**
     * Returns processed date
     * @return Date|null
     */
    public Date getProcessedDate()
    {
        return processedDate;
    }

    /**
     * Setter for processed date
     * @param processedDate Date
     */
    public void setProcessedDate(Date processedDate)
    {
        this.processedDate = processedDate;
    }

    /**
     * Returns due date
     * @return Date
     */
    public Date getDueDate()
    {
        return dueDate;
    }

    /**
     * Setter for due date
     * @param dueDate Date
     */
    public void setDueDate(Date dueDate)
    {
        this.dueDate = dueDate;
    }

    /**
     * Returns abbreviation for month title
     * @return String
     */
    public String getDueDateMonthString()
    {
        switch (this.getDueDate().getMonth())
        {
            case 0:
                return "LED";
            case 1:
                return "ÚNO";
            case 2:
                return "BŘE";
            case 3:
                return "DUB";
            case 4:
                return "KVĚ";
            case 5:
                return "ČVN";
            case 6:
                return "ČVC";
            case 7:
                return "SRP";
            case 8:
                return "ZÁŘ";
            case 9:
                return "ŘÍJ";
            case 10:
                return "LIS";
            case 11:
                return "PRO";
            default:
                return "";
        }
    }

    /**
     * Returns full account number:
     *      - Account number/Bank Code
     *
     * @return String
     */
    public String getFullAccountNumber(){
        return super.getBenefAccount() + "/" + super.getBenefBankCode();
    }

    /**
     * Returns result of recaptcha
     * @return boolean
     */
    public boolean isRecaptcha()
    {
        return recaptcha;
    }

    /**
     * Setter for result of recaptcha
     * @param recaptcha boolean
     */
    public void setRecaptcha(boolean recaptcha)
    {
        this.recaptcha = recaptcha;
    }
}
