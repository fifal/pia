package cz.filek.pia.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity for bank codes
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "bank_codes")
public class BankCode
{
    /**
     * Because ORM just werks - not >:(
     *  - can't set default value to boolean isActive
     */
    public BankCode()
    {
        this.isActive = true;
    }

    @Id
    @Column(name = "code", length = 4, unique = true)
    @Size(min = 3, max = 4, message = "Délka musí být {min} - {max} znaky.")
    @NotNull
    private String code;

    @Column(name = "name", length = 255)
    @Size(min = 5, max = 255, message = "Délka musí být {min} - {max} znaků.")
    private String name;

    @Column(name = "is_active")
    private boolean isActive;

    /**
     * Returns PK bank code in format: #### (e.g. 0800)
     * @return String
     */
    public String getCode()
    {
        return code;
    }

    /**
     * Setter for bank code
     * @param code String
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * Returns bank name
     * @return String
     */
    public String getName()
    {
        return name;
    }

    /**
     * Setter for bank name
     * @param name String
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Returns true if code is active
     * @return boolean
     */
    public boolean isActive()
    {
        return isActive;
    }

    /**
     * Setter for code active status
     * @param active boolean
     */
    public void setActive(boolean active)
    {
        isActive = active;
    }
}
