package cz.filek.pia.model;

import javax.persistence.*;

/**
 * Entity for user roles
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "user_roles")
public class UserRole
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    /**
     * Returns PK ID
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Returns user entity
     * @return User
     */
    public User getUser()
    {
        return user;
    }

    /**
     * Setter for user entity
     * @param user User
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * Returns role entity
     * @return Role
     */
    public Role getRole()
    {
        return role;
    }

    /**
     * Setter for role entity
     * @param role Role
     */
    public void setRole(Role role)
    {
        this.role = role;
    }
}
