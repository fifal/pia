package cz.filek.pia.model;

import cz.filek.pia.model.enums.ERole;
import cz.filek.pia.model.enums.PostgresSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

/**
 * Entity for role
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "role", schema = "public")
@TypeDef(name = "eRole", typeClass = PostgresSQLEnumType.class)
public class Role
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    @Type(type = "eRole")
    private ERole role;

    /**
     * Returns PK ID
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Returns role type
     * @return ERole enum
     */
    public ERole getRole()
    {
        return role;
    }

    /**
     * Setter for role type
     * @param role ERole enum
     */
    public void setRole(ERole role)
    {
        this.role = role;
    }
}
