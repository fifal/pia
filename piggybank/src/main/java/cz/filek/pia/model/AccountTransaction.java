package cz.filek.pia.model;

import cz.filek.pia.model.enums.ETransaction;
import cz.filek.pia.model.enums.PostgresSQLEnumType;
import cz.filek.pia.repository.AccountTransactionRepository;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.sql.Date;

/**
 * Entity for account transactions
 *
 * @author Filip Jani
 */
@Entity
@Table(name = "account_transactions")
@TypeDef(name = "eTransaction", typeClass = PostgresSQLEnumType.class)
public class AccountTransaction implements Comparable<AccountTransaction>
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    @Type(type = "eTransaction")
    private ETransaction transactionType;

    @OneToOne
    @JoinColumn(name = "payment_order_id")
    private PaymentOrder paymentOrder;

    @Column(name = "date")
    private Date date;

    /**
     * Returns transaction ID
     *
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Setter for ID
     *
     * @param id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Returns account entity
     *
     * @return Account
     */
    public Account getAccount()
    {
        return account;
    }

    /**
     * Setter for account
     *
     * @param accountId int
     */
    public void setAccount(Account accountId)
    {
        this.account = accountId;
    }


    /**
     * Returns transaction type
     * @return ETransaction
     */
    public ETransaction getTransactionType()
    {
        return transactionType;
    }

    /**
     * Setter for transaction type
     * @param transactionType ETransaction
     */
    public void setTransactionType(ETransaction transactionType)
    {
        this.transactionType = transactionType;
    }

    /**
     * Retruns payment order entity
     * @return PaymentOrder
     */
    public PaymentOrder getPaymentOrder()
    {
        return paymentOrder;
    }

    /**
     * Setter for payment order entity
     * @param paymentOrder PaymentOrder
     */
    public void setPaymentOrder(PaymentOrder paymentOrder)
    {
        this.paymentOrder = paymentOrder;
    }

    /**
     * Returns date of transaction
     *
     * @return Date
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * Setter for transaction date
     *
     * @param date Date
     */
    public void setDate(Date date)
    {
        this.date = date;
    }

    @Override
    public int compareTo(AccountTransaction accountTransaction)
    {
        return this.id > accountTransaction.id ? 1 : 0;
    }
}
