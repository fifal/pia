package cz.filek.pia.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

/**
 * Component for storing captcha keys
 *
 * @author Filip Jani
 */
@Component
@ConfigurationProperties(prefix = "google.recaptcha.key")
public class CaptchaConfig
{
    /**
     * ReCaptcha Site key
     */
    private String site;
    /**
     * ReCaptcha Secret key
     */
    private String secret;

    /**
     * Returns site key
     *
     * @return String
     */
    public String getSite()
    {
        return site;
    }

    /**
     * Setter for site key
     *
     * @param site String
     */
    public void setSite(String site)
    {
        this.site = site;
    }

    /**
     * Returns secret key
     *
     * @return String
     */
    public String getSecret()
    {
        return secret;
    }

    /**
     * Setter for secret key
     *
     * @param secret String
     */
    public void setSecret(String secret)
    {
        this.secret = secret;
    }

    /**
     * Client Http Request for validating captcha
     *
     * @return ClientHttpRequestFactory
     */
    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(3 * 1000);
        factory.setReadTimeout(7 * 1000);
        return factory;
    }

    /**
     * RestOperations for validating captcha
     *
     * @return RestOperations
     */
    @Bean
    public RestOperations restTemplate()
    {
        return new RestTemplate(this.clientHttpRequestFactory());
    }
}
