package cz.filek.pia.controller.admin;

import cz.filek.pia.config.CaptchaConfig;
import cz.filek.pia.controller.AuthController;
import cz.filek.pia.frontend.FlashMessages;
import cz.filek.pia.model.*;
import cz.filek.pia.services.*;
import cz.filek.pia.services.interfaces.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * Controller for Users in Administration
 * - allows only users with 'admin' role
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(value = "/admin", name = "UsersCtl")
public class UsersController extends AuthController
{
    private final Logger logger = LoggerFactory.getLogger(UsersController.class);

    private IUserService userService;
    private ITransactionsService transactionsService;
    private IAccountService accountService;
    private IMailService mailService;
    private CaptchaConfig captchaConfig;
    private ICaptchaService captchaService;

    public UsersController(UserService userService,
                           TransactionsService transactionsService,
                           AccountService accountService,
                           MailService mailService,
                           CaptchaConfig captchaConfig,
                           CaptchaService captchaService)
    {
        super(transactionsService, userService);
        this.userService = userService;
        this.transactionsService = transactionsService;
        this.accountService = accountService;
        this.mailService = mailService;
        this.captchaConfig = captchaConfig;
        this.captchaService = captchaService;
    }

    private final String USER_MODEL = "user";

    /**
     * Shows page with user list
     *
     * @return ModelAndView
     */
    @RequestMapping(path = "/users", name = "listUsers")
    public ModelAndView listUsers()
    {
        ModelAndView modelAndView = new ModelAndView("admin/users/list");

        List<User> users = userService.getUsersList();

        ModelMap modelMap = modelAndView.getModelMap();
        modelMap.put("users", users);

        return modelAndView;
    }

    /**
     * Shows page for adding new users
     *
     * @return ModelAndView
     */
    @RequestMapping(path = "/users/add", name = "addUser")
    public ModelAndView addUser()
    {
        return new ModelAndView("admin/users/add", USER_MODEL, new UserAddress()).addObject("captchaSite", captchaConfig.getSite());
    }

    /**
     * Handles adding of new users
     */
    @PostMapping(path = "/users/add/submit", name = "addUserSubmit")
    public ModelAndView addUserSubmit(@Valid @ModelAttribute(USER_MODEL) UserAddress userAddress,
                                      BindingResult result,
                                      final RedirectAttributes ra,
                                      HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("admin/users/add").addObject("captchaSite", captchaConfig.getSite());

        String captchaResponse = request.getParameter("g-recaptcha-response");
        if (!this.captchaService.processResponse(captchaResponse))
        {
            result.rejectValue("recaptcha", "error.userAddress", "Nepodařilo se ověřit captchu. Ujistěte se, že máte povolený JS.");
        }

        if (result.hasErrors())
        {
            return modelAndView;
        }

        try
        {
            User savedUser = this.userService.addNewUser(userAddress);

            FlashMessages.addInfo(ra, "Zákazník byl úspěšně přidán.");
            return new ModelAndView("redirect:/admin/users/edit/" + savedUser.getId());
        } catch (Exception ex)
        {
            FlashMessages.addError(modelAndView, "Nepodařilo se přidat zákazníka.");
            logger.warn("Nepodařilo se přidat zákazníka. " + ex.getMessage());

            return modelAndView;
        }
    }

    /**
     * Handles user edit form
     *
     * @param userAddress : UserAddress entity
     * @param result      : BindingResult
     * @param ra          : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(path = "/users/edit/submit", name = "editUserSubmit")
    public ModelAndView editUserSubmit(@Valid @ModelAttribute(USER_MODEL) UserAddress userAddress,
                                       BindingResult result,
                                       final RedirectAttributes ra,
                                       HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("admin/users/add").addObject("captchaSite", captchaConfig.getSite());;

        String captchaResponse = request.getParameter("g-recaptcha-response");
        if (!this.captchaService.processResponse(captchaResponse))
        {
            result.rejectValue("recaptcha", "error.userAddress", "Nepodařilo se ověřit captchu. Ujistěte se, že máte povolený JS.");
        }

        if (result.hasErrors())
        {
            return modelAndView;
        }

        try
        {
            UserAddress currentDetails = this.userService.getUserAddress(userAddress.getUser().getId());

            this.userService.updateUser(userAddress);

            this.mailService.sendMailAccountEdited(currentDetails.getUser().getEmail(), userAddress);

            FlashMessages.addInfo(ra, "Zákazník byl úspěšně upraven.");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Nepodařilo se upravit zákazníka.");
            logger.warn("Nepodařilo se upravit zákazníka. " + ex.getMessage());
        }

        return new ModelAndView("redirect:/admin/users/edit/" + userAddress.getUser().getId());
    }

    /**
     * Handles user deletion
     *
     * @param id : user's ID
     * @return ModelAndView
     */
    @GetMapping(path = "/users/delete/{id}", name = "deleteUser")
    public ModelAndView deleteUser(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/users");

        if (!this.userService.deleteUser(id))
        {
            FlashMessages.addError(ra, "Uživatel nebyl nalezen");
            return modelAndView;
        }

        FlashMessages.addInfo(ra, "Uživatel byl smazán");
        return modelAndView;
    }

    /**
     * Shows page for user editing
     *
     * @param id : user's ID
     * @return ModelAndView
     */
    @GetMapping(path = "/users/edit/{id}", name = "editUser")
    public ModelAndView editUser(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        UserAddress userAddress = this.userService.getUserAddress(id);
        if (userAddress == null)
        {
            ModelAndView modelAndView = new ModelAndView("redirect:/admin/users");
            FlashMessages.addError(ra, "Uživatel nebyl nalezen");
            return modelAndView;
        }

        List<UserAccount> userAccounts = this.userService.getUserAccounts(id);
        List<UserCard> userCards = this.userService.getUserCards(id);

        Map<Integer, Integer> balanceMap = new HashMap<>();
        for (UserAccount ua : userAccounts)
        {
            int balance = this.transactionsService.getAccountBalance(ua.getAccount().getId());
            balanceMap.put(ua.getAccount().getId(), balance);
        }

        ModelAndView editView = new ModelAndView("admin/users/add");
        editView.addObject("captchaSite", captchaConfig.getSite());
        editView.addObject("user", userAddress);
        editView.addObject("userAccounts", userAccounts);
        editView.addObject("userCards", userCards);
        editView.addObject("account", new Account());
        editView.addObject("accountBalance", balanceMap);
        editView.addObject("isEdit", true);
        editView.addObject("captchaSite", this.captchaConfig.getSite());
        return editView;
    }

    /**
     * Handles form for generating new accounts for users
     *
     * @param id : user's ID
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(path = "/users/new-account/{id}", name = "newAccount")
    public ModelAndView newAccount(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        User user = this.userService.getUser(id);
        if (user == null)
        {
            FlashMessages.addError(ra, "Uživatel nebyl nalezen");
            return new ModelAndView("redirect:/admin/users");
        }

        try
        {
            this.userService.generateUserAccount(user);

            FlashMessages.addInfo(ra, "Uživateli byl vytvořen nový účet");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Účet se nepodařilo vytvořit");
            logger.warn("Účet se nepodařilo vytvořit. " + ex.getMessage());
        }

        return new ModelAndView("redirect:/admin/users/edit/" + user.getId());
    }

    /**
     * Deletes account with given ID
     *
     * @param id : account's ID
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @GetMapping(path = "/users/delete-account/{id}", name = "deleteAccount")
    public ModelAndView deleteAccount(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        Account account = this.accountService.getAccount(id);
        if (account == null)
        {
            FlashMessages.addError(ra, "Účet nebyl nalezen");
            return new ModelAndView("redirect:/admin/users");
        }

        UserAccount userAccount = this.userService.getUserAccount(account.getId());

        this.accountService.deleteAccount(account);

        FlashMessages.addInfo(ra, "Účet byl úspěšně smazán.");

        return new ModelAndView("redirect:/admin/users/edit/" + userAccount.getUser().getId());
    }

    /**
     * Adds new card to user's account
     *
     * @return ModelAndView
     */
    @PostMapping(path = "/users/new-card/submit", name = "newCard")
    public ModelAndView newCard(HttpServletRequest request, RedirectAttributes ra)
    {
        ModelAndView modelAndView;
        try
        {
            int accountId = Integer.parseInt(request.getParameter("id"));
            int userId = Integer.parseInt(request.getParameter("userId"));

            this.userService.generateUserCard(userId, accountId);

            FlashMessages.addInfo(ra, "Karta byla úspěšně přidána");
            modelAndView = new ModelAndView("redirect:/admin/users/edit/" + userId);
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Nepodařilo se vytvořit novou platební kartu.");
            logger.warn("Nepodařilo se vytvořit novou platební kartu. " + ex.getMessage());

            modelAndView = new ModelAndView("redirect:/admin/users");
        }

        return modelAndView;
    }

    /**
     * Deletes card from user's account
     *
     * @param id : card's ID
     * @param ra : RedirectAttributes
     * @return RedirectAttributes
     */
    @GetMapping(path = "/users/delete-card/{id}", name = "deleteCard")
    public ModelAndView deleteCard(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        ModelAndView editView = new ModelAndView("redirect:/admin/users/edit/" + this.userService.getUserByCardId(id).getId());

        try
        {
            boolean result = this.userService.deleteCard(id);

            if (!result)
            {
                FlashMessages.addError(ra, "Karta nebyla nalezena");
                return new ModelAndView("redirect:/admin/users");
            }

            FlashMessages.addInfo(ra, "Karta byla smazána");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Kartu se nepodařilo smazat");
            logger.warn("Nepodařilo se smazat platební kartu. " + ex.getMessage());
        }

        return editView;
    }

    /**
     * Handles form for adding money to account
     *
     * @return ModelAndView
     */
    @PostMapping(path = "/users/top-up-account/submit", name = "topUpAccount")
    public ModelAndView topUpAccount(HttpServletRequest request, RedirectAttributes ra)
    {
        ModelAndView modelAndView = null;
        try
        {
            int userId = Integer.parseInt(request.getParameter("userId"));

            modelAndView = new ModelAndView("redirect:/admin/users/edit/" + userId);

            int accountId = Integer.parseInt(request.getParameter("id"));
            int amount = Integer.parseInt(request.getParameter("amount"));

            this.accountService.topUpAccount(accountId, amount);

            FlashMessages.addInfo(ra, "Na účet bylo přidáno " + amount + " pěnězů");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Na účet se nepodařílo přidat peníze.");
            logger.warn("Na účet se nepodařilo přidat peníze. " + ex.getMessage());

            if (modelAndView == null)
            {
                modelAndView = new ModelAndView("redirect:/admin/users");
            }
        }

        return modelAndView;
    }
}
