package cz.filek.pia.controller;

import cz.filek.pia.model.User;
import cz.filek.pia.model.UserAccount;
import cz.filek.pia.model.UserRole;
import cz.filek.pia.model.enums.ERole;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import cz.filek.pia.services.interfaces.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Auth controller used as a base for all controllers where user has to be logged in
 *
 * @author Filip Jani
 */
@Controller
public abstract class AuthController extends BaseController
{
    private IUserService userService;
    private ITransactionsService transactionsService;

    private User user;
    private UserRole userRole;
    private UserAccount userAccount;

    public AuthController(TransactionsService transactionsService, UserService userService)
    {
        this.transactionsService = transactionsService;
        this.userService = userService;
    }

    /**
     * Adds some commonly used attributes into the model
     *
     * @param model Model
     */
    @ModelAttribute
    public void addUserInfo(Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = this.userService.getUserByLogin(auth.getName());
        UserRole userRole = this.userService.getUserRole(user).get(0);

        if(userRole.getRole().getRole() == ERole.user)
        {
            UserAccount userAccount = this.userService.getUserAccounts(user.getId()).get(0);
            int balance = this.transactionsService.getAccountBalance(userAccount.getAccount().getId());

            Integer usableBalance = this.transactionsService.getAccountUsableBalance(userAccount.getAccount().getId());
            model.addAttribute("accountUsableBalance", usableBalance);
            model.addAttribute("userAccount", userAccount);
            model.addAttribute("accountBalance", balance);

            this.userAccount = userAccount;
        }

        if (user != null)
        {
            model.addAttribute("userEntity", user);
            model.addAttribute("auth", auth);

            this.user = user;
            this.userRole = userRole;
        }
    }

    /**
     * Returns User entity of logged in user
     *
     * @return
     */
    public User getUser()
    {
        return this.user;
    }

    /**
     * Returns UserAccount entity of logged in user
     *
     * @return UserAccount
     */
    public UserAccount getUserAccount()
    {
        return this.userAccount;
    }

    /**
     * Returns true if currently logged user is admin
     *
     * @return true if user is admin, false otherwise
     */
    boolean isUserAdmin()
    {
        return this.userRole.getRole().getRole() == ERole.admin;
    }
}
