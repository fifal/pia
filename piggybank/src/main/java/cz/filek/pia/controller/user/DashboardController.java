package cz.filek.pia.controller.user;

import cz.filek.pia.controller.AuthController;
import cz.filek.pia.model.*;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import cz.filek.pia.services.interfaces.ITransactionsService;
import cz.filek.pia.services.interfaces.IUserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.List;

/**
 * Controller for clients dashboard
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/user", name = "DashboardCtl")
public class DashboardController extends AuthController
{
    private ITransactionsService transactionsService;
    private IUserService userService;

    public DashboardController(TransactionsService transactionsService,
                               UserService userService)
    {
        super(transactionsService, userService);
        this.transactionsService = transactionsService;
        this.userService = userService;
    }

    /**
     * Displays default dashboard for client
     *
     * @return ModelAndView
     */
    @GetMapping(path = "dashboard", name = "dashboard")
    public ModelAndView dashboard()
    {
        ModelAndView modelAndView = new ModelAndView("customer/dashboard/default");
        ModelMap modelMap = modelAndView.getModelMap();
        String userLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.getUserByLogin(userLogin);

        List<AccountTransaction> accountTransactions = transactionsService.getAccountTransactions(this.getUserAccount().getAccount().getId());
        Collections.reverse(accountTransactions);
        modelMap.addAttribute("accountTransactions", accountTransactions);

        List<UserCard> userCards = userService.getUserCards(user.getId());
        modelMap.addAttribute("userCards", userCards);

        return modelAndView;
    }
}
