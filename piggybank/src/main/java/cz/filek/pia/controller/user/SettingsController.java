package cz.filek.pia.controller.user;

import cz.filek.pia.controller.AuthController;
import cz.filek.pia.frontend.FlashMessages;
import cz.filek.pia.model.UserAddress;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import cz.filek.pia.services.interfaces.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Settings controller for users where they may edit their contact information
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/user", name = "SettingsCtl")
public class SettingsController extends AuthController
{
    private final String USER_MODEL = "user";

    private IUserService userService;

    public SettingsController(UserService userService,
                              TransactionsService transactionsService)
    {
        super(transactionsService, userService);
        this.userService = userService;
    }

    /**
     * Shows form used for contact info editing
     *
     * @return ModelAndView
     */
    @GetMapping(name = "settings", path = "/settings")
    public ModelAndView userSettings()
    {
        ModelAndView modelAndView = new ModelAndView("customer/settings/default", USER_MODEL, new UserAddress());
        ModelMap modelMap = modelAndView.getModelMap();

        UserAddress userAddress = this.userService.getUserAddress(this.getUser().getId());

        modelMap.addAttribute(USER_MODEL, userAddress);

        return modelAndView;
    }

    /**
     * Processing of the submitted edit of user details
     *
     * @param userAddress : UserAddress model
     * @param result : BindingResult
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(name = "editSubmit", path = "/settings/submit")
    public ModelAndView userEditSubmit(@Valid @ModelAttribute(USER_MODEL) UserAddress userAddress,
                                       BindingResult result,
                                       final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("customer/settings/default");
        if (result.hasErrors())
        {
            return modelAndView;
        }

        try
        {
            this.userService.updateUserAddress(this.getUser(), userAddress);

            FlashMessages.addSuccess(ra, "Údaje byly upraveny.");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Nepodařilo se upravit údaje.");
        }

        return new ModelAndView("redirect:/user/settings");
    }
}
