package cz.filek.pia.controller;

import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;

/**
 * Controller used for showing page errors
 *
 * @author Filip Jani
 */
@Controller
public class ErrorController extends AuthController implements org.springframework.boot.web.servlet.error.ErrorController
{
    public ErrorController(TransactionsService transactionsService, UserService userService)
    {
        super(transactionsService, userService);
    }

    /**
     * Handles showing of correct error page based on HTTP response code
     *
     * @param response : HttpServletResponse
     * @return ModelAndView
     */
    @RequestMapping(path = "/error")
    public ModelAndView handleError(HttpServletResponse response)
    {
        switch (response.getStatus())
        {
            case HttpServletResponse.SC_INTERNAL_SERVER_ERROR:
            {
                return new ModelAndView("error/500").addObject("isAdmin", this.isUserAdmin());
            }
            case HttpServletResponse.SC_FORBIDDEN:
            {
                return new ModelAndView("error/403").addObject("isAdmin", this.isUserAdmin());
            }
            case HttpServletResponse.SC_NOT_FOUND:
            default:
            {
                return new ModelAndView("error/404").addObject("isAdmin", this.isUserAdmin());
            }
        }
    }

    @Override
    public String getErrorPath()
    {
        return null;
    }
}
