package cz.filek.pia.controller.user;

import cz.filek.pia.config.CaptchaConfig;
import cz.filek.pia.controller.AuthController;
import cz.filek.pia.frontend.FlashMessages;
import cz.filek.pia.frontend.Paginator;
import cz.filek.pia.model.*;
import cz.filek.pia.services.*;
import cz.filek.pia.services.interfaces.*;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.sql.Date;
import java.util.List;

/**
 * Controller used for managing of transactions
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/user", name = "TransactionCtl")
public class TransactionController extends AuthController
{
    private IBankCodeService bankCodeService;
    private IUserService userService;
    private ITransactionsService transactionsService;
    private IPaymentTemplateService paymentTemplateService;
    private IAccountService accountService;
    private CaptchaConfig captchaConfig;
    private ICaptchaService captchaService;

    private final String PAYMENT_ORDER = "paymentOrder";

    public TransactionController(BankCodeService bankCodeService,
                                 UserService userService,
                                 TransactionsService transactionsService,
                                 PaymentTemplateService paymentTemplateService,
                                 AccountService accountService,
                                 CaptchaConfig captchaConfig,
                                 CaptchaService captchaService)
    {
        super(transactionsService, userService);
        this.bankCodeService = bankCodeService;
        this.userService = userService;
        this.transactionsService = transactionsService;
        this.paymentTemplateService = paymentTemplateService;
        this.accountService = accountService;
        this.captchaConfig = captchaConfig;
        this.captchaService = captchaService;
    }

    /**
     * Shows form for submitting new transactions
     *
     * @return ModelAndView
     */
    @GetMapping(path = "/transaction/new", name = "new")
    public ModelAndView newTransaction()
    {
        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setDueDate(new Date(new java.util.Date().getTime()));

        ModelAndView modelAndView = new ModelAndView("customer/transaction/new", PAYMENT_ORDER, paymentOrder);
        modelAndView.addObject("captchaSite", this.captchaConfig.getSite());
        this.addCommon(modelAndView);

        return modelAndView;
    }

    /**
     * Shows form for submitting new transactions with pre-filled inputs from template by its ID
     *
     * @param id : ID of the template
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @GetMapping(path = "/transaction/new/{id}", name = "newFromTemplate")
    public ModelAndView newTransactionFromTemplate(@PathVariable("id") Integer id, final RedirectAttributes ra)
    {
        if (!this.paymentTemplateService.belongsTemplateToUser(id, this.getUser()))
        {
            FlashMessages.addError(ra, "Šablona nebyla nalezena.");
            return new ModelAndView("redirect:/transaction/new");
        }

        PaymentOrder paymentOrder = new PaymentOrder(this.paymentTemplateService.getPaymentOrderTemplate(id));

        ModelAndView modelAndView = new ModelAndView("customer/transaction/new", PAYMENT_ORDER, paymentOrder);
        modelAndView.addObject("selectedOption", id);
        modelAndView.addObject("captchaSite", this.captchaConfig.getSite());
        this.addCommon(modelAndView);

        return modelAndView;
    }

    /**
     * Handles form for submitting new transactions
     *
     * @param model : PaymentOrder
     * @param result : BindingResult
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(path = "/transaction/new/submit", name = "newSubmit")
    public ModelAndView newTransactionSubmit(@Valid @ModelAttribute(PAYMENT_ORDER) PaymentOrder model,
                                             BindingResult result,
                                             final RedirectAttributes ra,
                                             HttpServletRequest request)
    {
        ModelAndView modelAndView = new ModelAndView("customer/transaction/new");
        modelAndView.addObject("captchaSite", this.captchaConfig.getSite());
        this.addCommon(modelAndView);

        String captchaResponse = request.getParameter("g-recaptcha-response");
        if (!this.captchaService.processResponse(captchaResponse))
        {
            result.rejectValue("recaptcha", "error.userAddress", "Nepodařilo se ověřit captchu. Ujistěte se, že máte povolený JS.");
        }

        if(result.hasErrors())
        {
            return modelAndView;
        }

        Account account = this.getUserAccount().getAccount();
        if (model.getBenefAccount().equals(account.getNumber()) && model.getBenefBankCode().equals(account.getBankCode().getCode()))
        {
            FlashMessages.addError(modelAndView, "Nelze odesílat nové platby na vlastní účet.");
            return modelAndView;
        }

        Integer balance = this.transactionsService.getAccountUsableBalance(account.getId());
        if (model.getAmount() > balance)
        {
            FlashMessages.addError(modelAndView, "Na účtu nemáte dostatek peněz pro uskutečnění této transakce.");
            return modelAndView;
        }

        if (this.transactionsService.saveNewPaymentOrder(model, this.getUserAccount()))
        {
            FlashMessages.addSuccess(ra, "Platba byla přijata ke zpracování.");
        } else
        {
            FlashMessages.addError(ra, "Platbu se nepodařilo provést, opakujte prosím akci později.");
        }

        return new ModelAndView("redirect:/user/dashboard");
    }

    /**
     * Shows transaction detail
     *
     * @param id : ID of the transaction
     * @return ModelAndView
     */
    @GetMapping(path = "/transaction/detail/{id}", name = "transactionDetail")
    public ModelAndView showTransactionDetail(@PathVariable("id") int id)
    {
        ModelAndView modelAndView = new ModelAndView("customer/transaction/detail");
        AccountTransaction accountTransaction = this.transactionsService.getAccountTransaction(id);

        modelAndView.addObject("transaction", accountTransaction);

        return modelAndView;
    }

    /**
     * Shows list with all transactions
     *
     * @param page : current page
     * @param limit : items per page
     * @return ModelAndView
     */
    @GetMapping(path = "/transaction/history", name = "history")
    public ModelAndView showTransactionsHistory(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit){
        ModelAndView modelAndView = new ModelAndView("customer/transaction/history");

        if (page == null)
        {
            page = 1;
        }

        if (limit == null)
        {
            limit = Paginator.getLimits()[0];
        }

        Paginator paginator = new Paginator(page, limit, new Sort(Sort.Direction.DESC, "id"));
        List<AccountTransaction> accountTransactions = this.transactionsService.getAccountTransactions(this.getUserAccount(), paginator);
        paginator.prepareForFrontend(this.transactionsService.getAccountTransactionsCount(this.getUserAccount()));

        modelAndView.addObject("accountTransactions", accountTransactions);
        modelAndView.addObject("paginator", paginator);
        modelAndView.addObject("paginatorUrl", "TransactionCtl#history");

        return modelAndView;
    }


    /**
     * Returns JSON with payment order template, so we can fill form with correct values by AJAX
     *
     * @param accountNumber : ID of the payment order template
     * @return JSON payload
     */
    @GetMapping(path = "/transaction/check-account")
    @ResponseBody
    public boolean doesAccountExist(@RequestParam("number") String accountNumber)
    {
        return this.accountService.doesAccountExist(accountNumber);
    }

    //
    //
    //
    //
    //

    /**
     * Adds bank codes list into the template
     *
     * @param modelAndView ModelAndView
     */
    private void addCommon(ModelAndView modelAndView)
    {
        ModelMap map = modelAndView.getModelMap();

        List<BankCode> bankCodeList = bankCodeService.getBankCodes(true);
        map.addAttribute("bankCodeList", bankCodeList);

        List<UserPaymentTemplate> templates = userService.getUserPaymentTemplates(this.getUser());
        map.addAttribute("templates", templates);
    }
}
