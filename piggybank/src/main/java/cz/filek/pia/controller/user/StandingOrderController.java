package cz.filek.pia.controller.user;

import cz.filek.pia.controller.AuthController;
import cz.filek.pia.frontend.FlashMessages;
import cz.filek.pia.model.BankCode;
import cz.filek.pia.model.StandingOrder;
import cz.filek.pia.model.UserStandingOrder;
import cz.filek.pia.services.BankCodeService;
import cz.filek.pia.services.StandingOrderService;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.UserService;
import cz.filek.pia.services.interfaces.IBankCodeService;
import cz.filek.pia.services.interfaces.IStandingOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller for managing Standing Orders
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/user", name = "StandingOrderCtl")
public class StandingOrderController extends AuthController
{
    private IStandingOrderService standingOrderService;
    private IBankCodeService bankCodeService;

    private final String STANDING_ORDER = "standingOrder";

    public StandingOrderController(TransactionsService transactionsService,
                                   UserService userService,
                                   StandingOrderService standingOrderService,
                                   BankCodeService bankCodeService)
    {
        super(transactionsService, userService);
        this.standingOrderService = standingOrderService;
        this.bankCodeService = bankCodeService;
    }

    /**
     * Shows list with standing orders
     *
     * @return ModelAndView
     */
    @GetMapping(value = "/standing", name = "list")
    public ModelAndView listStandingOrders()
    {
        ModelAndView modelAndView = new ModelAndView("customer/standing/list");
        List<UserStandingOrder> userStandingOrders = this.standingOrderService.getUserStandingOrders(this.getUser());

        modelAndView.addObject("userStandingOrders", userStandingOrders);

        return modelAndView;
    }

    /**
     * Shows form for adding new standing order
     *
     * @return ModelAndView
     */
    @GetMapping(value = "/standing/add", name = "addOrder")
    public ModelAndView addStandingOrder()
    {
        ModelAndView modelAndView = new ModelAndView("customer/standing/edit", STANDING_ORDER, new StandingOrder());
        this.addCommon(modelAndView);


        return modelAndView;
    }

    /**
     * Processing of adding new standing order
     *
     * @param model : StandingOrder
     * @param result : BindingResult
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(value = "/standing/add", name = "addOrderSubmit")
    public ModelAndView addStandingOrderSubmit(@Valid @ModelAttribute(STANDING_ORDER) StandingOrder model,
                                               BindingResult result,
                                               final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("customer/standing/edit", STANDING_ORDER, model);

        this.validateDates(result, model);

        if (result.hasErrors())
        {
            this.addCommon(modelAndView);
            return modelAndView;
        }

        UserStandingOrder userStandingOrder = this.standingOrderService.updateOrSave(model, this.getUser(), this.getUserAccount());
        if (userStandingOrder != null)
        {
            FlashMessages.addSuccess(ra, "Trvalý příkaz byl úspěšně přidán.");
        } else
        {
            FlashMessages.addError(ra, "Trvalý příkaz se nepodařilo přidat.");
        }

        return new ModelAndView("redirect:/user/standing/");
    }

    /**
     * Shows edit form for existing standing order
     *
     * @param id : ID of the standing order
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @GetMapping(path = "/standing/edit/{id}", name = "editOrder")
    public ModelAndView editOrder(@PathVariable int id, final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("customer/standing/edit", STANDING_ORDER, new StandingOrder());

        if (!this.standingOrderService.belongsStandingOrderToUser(id, this.getUser()))
        {
            FlashMessages.addError(ra, "Trvalý příkaz nebyl nalezen.");
            return new ModelAndView("redirect:/user/standing");
        }


        StandingOrder standingOrder = this.standingOrderService.getStandingOrder(id);
        this.addCommon(modelAndView);
        modelAndView.addObject(STANDING_ORDER, standingOrder);

        return modelAndView;
    }

    /**
     * Processing of edit form for standing order
     *
     * @param id : ID of the template
     * @param model : PaymentOrderTemplate
     * @param result : BindingResult
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @PostMapping(path = "/standing/edit/{id}", name = "editOrderSubmit")
    public ModelAndView editOrderSubmit(@PathVariable int id,
                                           @Valid @ModelAttribute(STANDING_ORDER) StandingOrder model,
                                           BindingResult result,
                                           final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("customer/standing/edit");

        if (!this.standingOrderService.belongsStandingOrderToUser(id, this.getUser()))
        {
            FlashMessages.addError(ra, "Trvalý příkaz nebyl nalezen.");
            return new ModelAndView("redirect:/user/standing");
        }

        this.validateDates(result, model);

        if (result.hasErrors())
        {
            this.addCommon(modelAndView);
            return modelAndView;
        }

        try
        {
            this.standingOrderService.updateOrSave(model, this.getUser(), this.getUserAccount());
            FlashMessages.addSuccess(ra, "Trvalý příkaz byl úspěšně upraven.");
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Trvalý příkaz se nepodařilo uložit.");
        }

        return new ModelAndView("redirect:/user/standing/edit/" + id);
    }

    /**
     * Handles deletion of standing orders
     *
     * @param id : ID of the standing order
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @GetMapping(value = "/standing/delete/{id}", name = "deleteOrder")
    public ModelAndView deleteStandingOrder(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("redirect:/user/standing");

        try
        {
            if (this.standingOrderService.deleteStandingOrder(id, this.getUser()))
            {
                FlashMessages.addSuccess(ra, "Trvalý příkaz byl odstraněn.");
            } else
            {
                FlashMessages.addError(ra, "Trvalý příkaz se nepodařilo odstranit.");
            }
        } catch (Exception ex)
        {
            FlashMessages.addError(ra, "Trvalý příkaz se nepodařilo odstranit.");
        }

        return modelAndView;
    }

    /**
     * Changes state of standing order
     *  - if order is active -> set to NOT active
     *  - if order is NOT active -> set to active
     *
     * @param id : ID of the standing order
     * @param ra : RedirectAttributes
     * @return ModelAndView
     */
    @GetMapping(value = "/standing/change-state/{id}", name = "switchState")
    public ModelAndView changeState(@PathVariable("id") int id, final RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("redirect:/user/standing");

        if (this.standingOrderService.switchState(id, this.getUser()))
        {
            FlashMessages.addSuccess(ra, "Aktivita trvalého příkazu byla pozměněna.");
        } else
        {
            FlashMessages.addError(ra, "Aktivita trvalého příkazu se nepodařilo změnit.");
        }

        return modelAndView;
    }

    //
    //
    //
    //
    //

    /**
     * Adds common variables into the model
     *
     * @param modelAndView ModelAndView
     */
    private void addCommon(ModelAndView modelAndView)
    {
        List<BankCode> bankCodeList = this.bankCodeService.getBankCodes(true);
        modelAndView.addObject("bankCodeList", bankCodeList);
    }

    /**
     * Checks if start standing order date is < stop standing order date
     *
     * @param result : BindingResult
     * @param order : StandingOrder
     */
    private void validateDates(BindingResult result, StandingOrder order)
    {
        if (order.getStandingStartDate().getTime() > order.getStandingStopDate().getTime())
        {
            result.rejectValue("standingStartDate", "error.standingOrder", "Hodnota konce odesílání musí být větší než počátku.");
        }
    }
}
