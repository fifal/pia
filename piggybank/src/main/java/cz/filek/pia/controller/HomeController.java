package cz.filek.pia.controller;

import cz.filek.pia.model.enums.ERole;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;

/**
 * Controller used to redirect users by their role
 *
 * @author Filip Jani
 */
@Controller
@RequestMapping(path = "/", name = "HomeCtl")
public class HomeController extends BaseController
{
    /**
     * Redirects user
     *  - if user is already logged in it will redirect him to corresponding page by his user role
     *  - if user is not logged, it will redirect him to login page
     *
     * @return ModelAndView
     */
    @RequestMapping(path = "/", name = "homePage")
    public ModelAndView showHomepage()
    {
        Authentication authenticator = SecurityContextHolder.getContext().getAuthentication();

        if (hasRole(authenticator.getAuthorities(), ERole.admin))
        {
            return new ModelAndView("redirect:/admin");
        } else if (hasRole(authenticator.getAuthorities(), ERole.user))
        {
            return new ModelAndView("redirect:/user/dashboard");
        } else
        {
            return new ModelAndView("redirect:/login");
        }
    }

    /**
     * Checks if user is in role
     *
     * @param authorities : Collection<? extends GrantedAuthority>
     * @param role : ERole
     * @return true if user is in role, false if not
     */
    private boolean hasRole(Collection<? extends GrantedAuthority> authorities, ERole role)
    {
        for (GrantedAuthority auth : authorities)
        {
            if (auth.getAuthority().equals(role.toString()))
            {
                return true;
            }
        }

        return false;
    }
}