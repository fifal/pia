package cz.filek.pia.services;

import cz.filek.pia.model.BankCode;
import cz.filek.pia.repository.BankCodeRepository;
import cz.filek.pia.services.interfaces.IBankCodeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Service for managing bank codes
 *
 * @author Filip Jani
 */
@Service
public class BankCodeService implements IBankCodeService
{
    @Value("${piggybank.bankcodesource.url}")
    private String sourceUrl;

    @Value("${piggybank.bankcodesource.separator}")
    private String separator;

    private BankCodeRepository bankCodeRepository;

    /**
     * Constructor
     *
     * @param bankCodeRepository : BankCodeRepository
     */
    public BankCodeService(BankCodeRepository bankCodeRepository)
    {
        this.bankCodeRepository = bankCodeRepository;
    }

    /**
     * Returns list of bank codes by is_active column
     *
     * @param isActive : boolean
     * @return List<BankCode>
     */
    @Override
    public List<BankCode> getBankCodes(boolean isActive)
    {
        return this.bankCodeRepository.findByIsActiveOrderByCodeAsc(isActive);
    }

    /**
     * Returns BankCode by code
     * @param code : String
     * @return BankCode
     */
    @Override
    public BankCode getBankCode(String code)
    {
        return this.bankCodeRepository.findByCode(code);
    }

    /**
     * Saves new or updates existing bank code
     *
     * @param bankCode : BankCode
     */
    @Override
    public void saveOrUpdate(BankCode bankCode)
    {
        BankCode current = this.getBankCode(bankCode.getCode());

        // If code doesn't exist, create new one
        if (current == null)
        {
            bankCodeRepository.save(bankCode);
        } else
        {
            current.setName(bankCode.getName());
            current.setCode(bankCode.getCode());
            bankCodeRepository.save(current);
        }
    }

    /**
     * Synchronizes bank codes from external resource. Accepts CSV file which has first two columns in this format
     *
     *      int code; string name;
     *
     * @return true if synchronization was successful, false otherwise
     */
    @Override
    public boolean updateBankCodes()
    {
        try
        {
            URL url = new URL(sourceUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            int status = connection.getResponseCode();

            if (status == HttpURLConnection.HTTP_OK)
            {
                InputStream inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("windows-1250")));

                ArrayList<BankCode> bankCodes = new ArrayList<>();
                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    String[] splitted = line.split(separator);

                    // Code is not a number - skip
                    if (!isNumber(splitted[0]))
                    {
                        continue;
                    }

                    String code = splitted[0];
                    String name = splitted[1];

                    BankCode bankCode = new BankCode();
                    bankCode.setActive(true);
                    bankCode.setCode(String.valueOf(code));
                    bankCode.setName(name);

                    bankCodes.add(bankCode);
                }

                for (BankCode bankCode : bankCodes)
                {
                    this.saveOrUpdate(bankCode);
                }

            } else
            {
                return false;
            }

        } catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    /**
     * Checks if string can be parsed as integer
     *
     * @param code : String
     * @return true if is number, false otherwise
     */
    private boolean isNumber(String code)
    {
        try
        {
            Integer.parseInt(code);
        } catch (NumberFormatException ex)
        {
            return false;
        }

        return true;
    }
}
