package cz.filek.pia.services.interfaces;

import cz.filek.pia.model.BankCode;

import java.util.List;

/**
 * Interface for bank code service
 *
 * @author Filip Jani
 */
public interface IBankCodeService
{
    /**
     * Returns list of bank codes based on isActive
     * @param isActive : Is bank code active in database
     * @return List<BankCode>
     */
    List<BankCode> getBankCodes(boolean isActive);

    /**
     * Returns Bank Code entity by code
     * @param code : code of the bank
     * @return BankCode
     */
    BankCode getBankCode(String code);

    /**
     * Saves new bank code to database or updates already existing
     * @param bankCode : BankCode
     */
    void saveOrUpdate(BankCode bankCode);

    /**
     * Updates all bank codes from external source
     * @return true if synchronization was successful, false otherwise
     */
    boolean updateBankCodes();
}
