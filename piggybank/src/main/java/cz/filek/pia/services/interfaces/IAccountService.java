package cz.filek.pia.services.interfaces;

import cz.filek.pia.model.Account;

/**
 * Interface for account service
 *
 * @author Filip Jani
 */
public interface IAccountService
{
    /**
     * Adds amount of money to account with ID accountId
     * @param accountId : ID of the account
     * @param amount : amount of money
     * @return true if successful, false otherwise
     */
    boolean topUpAccount(int accountId, int amount);

    /**
     * Returns Account by account ID
     * @param accountId : ID of the account
     * @return Account
     */
    Account getAccount(int accountId);

    /**
     * Deletes account
     * @param account : Account entity
     * @return true if successful, false otherwise
     */
    boolean deleteAccount(Account account);

    /**
     * Checks if account with accountNumber exists at our bank
     * @param accountNumber : Number of the account
     * @return true if account already exists, false otherwise
     */
    boolean doesAccountExist(String accountNumber);
}
