package cz.filek.pia.services;

import cz.filek.pia.model.PaymentOrderTemplate;
import cz.filek.pia.model.User;
import cz.filek.pia.model.UserAccount;
import cz.filek.pia.model.UserPaymentTemplate;
import cz.filek.pia.repository.PaymentOrderTemplateRepository;
import cz.filek.pia.repository.UserPaymentTemplatesRepository;
import cz.filek.pia.services.interfaces.IPaymentTemplateService;
import org.springframework.stereotype.Service;

/**
 * Service for managing payment templates
 *
 * @author Filip Jani
 */
@Service
public class PaymentTemplateService implements IPaymentTemplateService
{
    private PaymentOrderTemplateRepository paymentOrderTemplateRepository;
    private UserPaymentTemplatesRepository userPaymentTemplatesRepository;

    public PaymentTemplateService(PaymentOrderTemplateRepository paymentOrderTemplateRepository,
                                  UserPaymentTemplatesRepository userPaymentTemplatesRepository)
    {
        this.paymentOrderTemplateRepository = paymentOrderTemplateRepository;
        this.userPaymentTemplatesRepository = userPaymentTemplatesRepository;
    }

    /**
     * Returns PaymentOrderTemplate by its ID
     *
     * @param id : ID of the paymentOrderTemplate
     * @return PaymentOrderTemplate
     */
    public PaymentOrderTemplate getPaymentOrderTemplate(int id)
    {
        return this.paymentOrderTemplateRepository.findById(id);
    }

    /**
     * Updates PaymentOrderTemplate
     *
     * @param paymentOrderTemplate : PaymentOrderTemplate
     * @param id : ID of the template when editing
     * @param userAccount : UserAccount of the user
     * @return true if successful, false otherwise
     */
    @Override
    public boolean updatePaymentOrderTemplate(PaymentOrderTemplate paymentOrderTemplate, int id, UserAccount userAccount)
    {
        paymentOrderTemplate.setId(id);
        paymentOrderTemplate.setAccount(userAccount.getAccount());

        this.paymentOrderTemplateRepository.save(paymentOrderTemplate);

        return true;
    }

    /**
     * Adds new PaymentOrderTemplate
     *
     * @param paymentOrderTemplate : Model with template
     * @param userAccount : UserAccount
     * @return created PaymentOrderTemplate
     */
    @Override
    public PaymentOrderTemplate addPaymentOrderTemplate(PaymentOrderTemplate paymentOrderTemplate, UserAccount userAccount)
    {
        paymentOrderTemplate.setAccount(userAccount.getAccount());

        PaymentOrderTemplate pot = this.paymentOrderTemplateRepository.save(paymentOrderTemplate);

        UserPaymentTemplate userPaymentTemplate = new UserPaymentTemplate();
        userPaymentTemplate.setPaymentOrderTemplate(pot);
        userPaymentTemplate.setUser(userAccount.getUser());

        this.userPaymentTemplatesRepository.save(userPaymentTemplate);

        return pot;
    }

    /**
     * Returns UserPaymentTemplate by PaymentOrderTemplate ID
     *
     * @param paymentTemplateId : ID of the PaymentOrderTemplate
     * @return UserPaymentTemplate
     */
    public UserPaymentTemplate getUserPaymentTemplate(int paymentTemplateId)
    {
        return this.userPaymentTemplatesRepository.findByPaymentOrderTemplateId(paymentTemplateId);
    }

    /**
     * Checks if template belongs to User
     *
     * @param templateId : ID of the PaymentOrderTemplate
     * @return true if does, false otherwise
     */
    @Override
    public boolean belongsTemplateToUser(int templateId, User user)
    {
        UserPaymentTemplate userPaymentTemplate = getUserPaymentTemplate(templateId);
        if (userPaymentTemplate == null)
        {
            return false;
        }

        return userPaymentTemplate.getUser().getId() == user.getId();
    }


    /**
     * Deletes PaymentOrderTemplate from database
     *
     * @param templateId : ID of the template
     * @return true if successful, false otherwise
     */
    @Override
    public boolean deletePaymentOrderTemplate(int templateId, User user)
    {
        try
        {
            if (!belongsTemplateToUser(templateId, user))
            {
                return false;
            }

            PaymentOrderTemplate paymentOrderTemplate = this.paymentOrderTemplateRepository.findById(templateId);
            this.paymentOrderTemplateRepository.delete(paymentOrderTemplate);

            return true;
        } catch (Exception ex)
        {
            return false;
        }
    }
}
