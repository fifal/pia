package cz.filek.pia.services;

import cz.filek.pia.model.*;
import cz.filek.pia.model.enums.ETransaction;
import cz.filek.pia.repository.*;
import cz.filek.pia.services.interfaces.ITransactionsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class TransactionsService implements ITransactionsService
{
    private PaymentOrderRepository paymentOrderRepository;
    private AccountTransactionRepository accountTransactionRepository;
    private AccountRepository accountRepository;
    private BankCodeRepository bankCodeRepository;

    @Value("${piggybank.bankcode}")
    private String bankCode;

    public TransactionsService(PaymentOrderRepository paymentOrderRepository,
                               AccountTransactionRepository accountTransactionRepository,
                               AccountRepository accountRepository,
                               BankCodeRepository bankCodeRepository)
    {
        this.paymentOrderRepository = paymentOrderRepository;
        this.accountTransactionRepository = accountTransactionRepository;
        this.accountRepository = accountRepository;
        this.bankCodeRepository = bankCodeRepository;
    }

    /**
     * Method processes all payment orders
     *  - withhold money from user's account
     *  - adds new record to account_transactions table
     *
     * @return boolean : true if operation was successful, false otherwise
     */
    @Override
    public boolean processPaymentOrders()
    {
        List<PaymentOrder> unprocessedList = paymentOrderRepository.findAllByProcessedDateIsNull();
        boolean isOk = true;

        for (PaymentOrder paymentOrder : unprocessedList)
        {
            int balance = accountTransactionRepository.findAccountBalanceByAccountId(paymentOrder.getAccount().getId());
            Date processedDate = new Date(System.currentTimeMillis());

            if (balance < Math.abs(paymentOrder.getAmount()))
            {
                isOk = false;
                continue;
            }

            // Withhold money
            saveNewAccountTransaction(paymentOrder.getAccount(), processedDate, paymentOrder, ETransaction.bank);

            // Process payment order
            saveAndProcessPaymentOrder(paymentOrder);

            // If benef. account is maintained at our bank
            if (paymentOrder.getBenefBankCode().equals(this.bankCode))
            {
                BankCode bankCode = bankCodeRepository.findByCodeAndIsActive(paymentOrder.getBenefBankCode(), true);
                Account benefAccount = accountRepository.findByNumberAndBankCode(paymentOrder.getBenefAccount(), bankCode);

                // Account was not found
                if (benefAccount == null)
                {
                    continue;
                }

                // Add transaction to benef. account
                saveNewAccountTransaction(benefAccount, processedDate, paymentOrder, ETransaction.bank);
            }
        }

        return isOk;
    }

    /**
     * Returns account balance by account ID
     *
     * @param accountId : ID of the account
     * @return int balance of the account
     */
    @Override
    public Integer getAccountBalance(int accountId)
    {
        Integer balance = this.accountTransactionRepository.findAccountBalanceByAccountId(accountId);

        return balance == null ? 0 : balance;
    }

    /**
     * Returns account usable balance by account ID
     *      usable = sum of transactions - payment orders which hasn't been processed yet
     *
     * @param accountId : ID of the account
     * @return int balance of the account
     */
    @Override
    public Integer getAccountUsableBalance(int accountId)
    {
        Integer balance = this.getAccountBalance(accountId);

        List<PaymentOrder> paymentOrders = this.paymentOrderRepository.findAllByAccountIdAndProcessedDateIsNull(accountId);
        if (paymentOrders == null)
        {
            return balance;
        }

        Integer paymentSum = 0;
        for (PaymentOrder po: paymentOrders)
        {
            paymentSum -= Math.abs(po.getAmount());
        }

        return balance - Math.abs(paymentSum);
    }

    /**
     * Saves new Payment order into database
     *
     * @param paymentOrder : PaymentOrder
     * @param userAccount : UserAccount
     * @return true if successful, false otherwise
     */
    @Override
    public boolean saveNewPaymentOrder(PaymentOrder paymentOrder, UserAccount userAccount)
    {
        paymentOrder.setAccount(userAccount.getAccount());
        try
        {
            paymentOrderRepository.save(paymentOrder);
        } catch (Exception ex)
        {
            return false;
        }

        return true;
    }
    
    /**
     * Returns account transaction by transaction ID
     *
     * @param transactionId : ID of the transaction
     * @return AccountTransaction
     */
    @Override
    public AccountTransaction getAccountTransaction(int transactionId)
    {
        return this.accountTransactionRepository.findById(transactionId);
    }

    /**
     * Returns list of account transactions by account ID limited by Pageable
     *
     * @param userAccount : UserAccount entity
     * @param pageable : Pageable
     * @return List<AccountTransaction>
     */
    @Override
    public List<AccountTransaction> getAccountTransactions(UserAccount userAccount, Pageable pageable)
    {
        return this.accountTransactionRepository.findAccountTransactionsByAccountId(userAccount.getAccount().getId(), pageable);
    }

    /**
     * Returns count of transactions for given account
     *
     * @param userAccount : UserAccount entity
     * @return Integer
     */
    @Override
    public Integer getAccountTransactionsCount(UserAccount userAccount)
    {
        return this.accountTransactionRepository.countAllByAccountId(userAccount.getAccount().getId());
    }

    /**
     * Returns list of account transactions by account ID
     *
     * @param accountId : ID of the account
     * @return List<AccountTransaction>
     */
    @Override
    public List<AccountTransaction> getAccountTransactions(int accountId)
    {
        return this.accountTransactionRepository.findAccountTransactionsByAccountId(accountId);
    }

    /**
     * Returns count of all transactions
     *
     * @return Integer
     */
    @Override
    public Integer getAccountTransactionsCount()
    {
        return this.accountTransactionRepository.countAll();
    }

    /**
     * Returns count of unprocessed payment orders
     *
     * @return Integer
     */
    @Override
    public Integer getUnprocessedPaymentsCount()
    {
        return this.paymentOrderRepository.countAllByProcessedDateIsNull();
    }

    /**
     * Creates new account transaction and saves it into account_transactions table
     *
     * @param account Account
     * @param date Date
     * @param paymentOrder PaymentOrder
     * @param transaction ETransaction
     * @return boolean : true if was saved correctly, false otherwise
     */
    private boolean saveNewAccountTransaction(Account account, Date date, PaymentOrder paymentOrder, ETransaction transaction)
    {
        boolean isOk = true;

        try
        {
            AccountTransaction accountTransaction = new AccountTransaction();
            accountTransaction.setAccount(account);
            accountTransaction.setDate(date);
            accountTransaction.setPaymentOrder(paymentOrder);
            accountTransaction.setTransactionType(transaction);

            accountTransactionRepository.save(accountTransaction);
        } catch (Exception ex)
        {
            isOk = false;
        }

        return isOk;
    }

    /**
     * Method sets payment order processed date and sets the right amount of money
     *
     * @param paymentOrder PaymentOrder
     * @return boolean : true if was saved correctly, false otherwise
     */
    private boolean saveAndProcessPaymentOrder(PaymentOrder paymentOrder)
    {
        boolean isOk = true;

        try
        {
            paymentOrder.setProcessedDate(new Date(System.currentTimeMillis()));
            paymentOrder.setAmount(-Math.abs(paymentOrder.getAmount()));
            paymentOrderRepository.save(paymentOrder);
        } catch (Exception ex)
        {
            isOk = false;
        }

        return isOk;
    }
}
