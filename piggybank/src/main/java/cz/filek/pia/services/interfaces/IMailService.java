package cz.filek.pia.services.interfaces;

import cz.filek.pia.model.StandingOrder;
import cz.filek.pia.model.UserAccount;
import cz.filek.pia.model.UserAddress;

/**
 * Interface for mail service
 *
 * @author Filip Jani
 */
public interface IMailService
{
    /**
     * Sends an email to client with his login credentials
     *
     * @param mailTo : Email of the client
     * @param login  : Login number of the client
     * @param pin    : Password of the client
     * @return true if mail was sent successfully, false otherwise
     */
    boolean sendMailAccountCreated(String mailTo, String login, String pin);

    /**
     * Sends an email to client with information that his account was deleted
     *
     * @param mailTo : Email of the client
     * @param userAccount : UserAccount entity
     * @return true if mail was sent successfully, false otherwise
     */
    boolean sendMailAccountDeleted(String mailTo, UserAccount userAccount);

    /**
     * Sends an email to client with information about changed details of his account by administrator
     *
     * @param mailTo : Email of the client
     * @param userAddress : Array of changes
     * @return true if mail was sent successfully, false otherwise
     */
    boolean sendMailAccountEdited(String mailTo, UserAddress userAddress);

    /**
     * Sends an email to client with information that his standing order couldn't be processed,
     * because he doesn't have enough money on the account.
     *
     * @param mailTo : Email of the client
     * @param standingOrder : StandingOrder entity
     * @param balance : User's account balance
     * @return true if mail was sent successfully, false otherwise
     */
    boolean sendMailStandingOrder(String mailTo, StandingOrder standingOrder, Integer balance);
}
