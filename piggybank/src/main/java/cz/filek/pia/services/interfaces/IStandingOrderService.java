package cz.filek.pia.services.interfaces;

import cz.filek.pia.model.StandingOrder;
import cz.filek.pia.model.User;
import cz.filek.pia.model.UserAccount;
import cz.filek.pia.model.UserStandingOrder;

import java.util.List;

/**
 * Interface for StandingOrderService
 *
 * @author Filip Jani
 */
public interface IStandingOrderService
{
    /**
     * Updates or adds new standing order and assigns it to user
     *
     * @param standingOrder : StandingOrder
     * @param user : User entity
     * @param userAccount : UserAccount entity
     * @return UserStandingOrder
     */
    UserStandingOrder updateOrSave(StandingOrder standingOrder, User user, UserAccount userAccount);

    /**
     * Returns list of all user standing orders
     *
     * @param user : User entity
     * @return List<UserStandingOrder>
     */
    List<UserStandingOrder> getUserStandingOrders(User user);

    /**
     * Returns standing order by its ID
     *
     * @param id : ID of the standing order
     * @return StandingOrder
     */
    StandingOrder getStandingOrder(int id);

    /**
     * Deletes standing order by ID
     *
     * @param id : ID of the standing order
     * @param user : User entity
     * @return true if delete was successful, false otherwise
     */
    boolean deleteStandingOrder(int id, User user);

    /**
     * Returns true if standing order with id belongs to user
     *
     * @param id : ID of the standing order
     * @param user : User entity
     * @return true if belongs, false otherwise
     */
    boolean belongsStandingOrderToUser(int id, User user);

    /**
     * Method will process all active standing orders
     *
     * @return true if successful, false otherwise
     */
    boolean processStandingOrders();

    /**
     * Switches state of standing order
     *  - active <=> not active
     *
     * @param id : ID of the standing order
     * @return boolean
     */
    boolean switchState(int id, User user);
}
