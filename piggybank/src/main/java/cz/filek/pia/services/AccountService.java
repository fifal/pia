package cz.filek.pia.services;

import cz.filek.pia.model.Account;
import cz.filek.pia.model.AccountTransaction;
import cz.filek.pia.model.BankCode;
import cz.filek.pia.model.PaymentOrder;
import cz.filek.pia.model.enums.ETransaction;
import cz.filek.pia.repository.AccountRepository;
import cz.filek.pia.repository.AccountTransactionRepository;
import cz.filek.pia.repository.BankCodeRepository;
import cz.filek.pia.repository.PaymentOrderRepository;
import cz.filek.pia.services.interfaces.IAccountService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Date;

/**
 * Service for managing accounts
 *
 * @author Filip Jani
 */
@Service
public class AccountService implements IAccountService
{
    @Value("${piggybank.bankcode}")
    private String bankCode;

    private PaymentOrderRepository paymentOrderRepository;
    private AccountTransactionRepository accountTransactionRepository;
    private AccountRepository accountRepository;
    private BankCodeRepository bankCodeRepository;

    public AccountService(PaymentOrderRepository paymentOrderRepository,
                          AccountTransactionRepository accountTransactionRepository,
                          AccountRepository accountRepository,
                          BankCodeRepository bankCodeRepository)
    {
        this.paymentOrderRepository = paymentOrderRepository;
        this.accountTransactionRepository = accountTransactionRepository;
        this.accountRepository = accountRepository;
        this.bankCodeRepository = bankCodeRepository;
    }

    /**
     * Method which tops up Account with accountId with given amount
     *
     * @param accountId : ID of the account
     * @param amount : amount of money (may be also negative - for money withdraw)
     * @return boolean : true if successful, false otherwise
     */
    @Override
    public boolean topUpAccount(int accountId, int amount)
    {
        Account account = this.accountRepository.findById(accountId);

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setAccount(account);
        paymentOrder.setBenefAccount(account.getNumber());
        paymentOrder.setBenefBankCode(account.getBankCode().getCode());
        paymentOrder.setConstSymbol("MNLPGY");
        paymentOrder.setSpecSymbol("MNLPGY");
        paymentOrder.setVarSymbol("MNLPGY");
        paymentOrder.setMessage("Manuální dobití účtu z administrace.");
        paymentOrder.setDueDate(new Date(System.currentTimeMillis()));
        paymentOrder.setAmount(amount);
        paymentOrder.setProcessedDate(new Date(System.currentTimeMillis()));

        AccountTransaction accountTransaction = new AccountTransaction();
        accountTransaction.setAccount(account);
        accountTransaction.setPaymentOrder(paymentOrder);
        accountTransaction.setTransactionType(ETransaction.bank);
        accountTransaction.setDate(new Date(System.currentTimeMillis()));

        this.paymentOrderRepository.save(paymentOrder);
        this.accountTransactionRepository.save(accountTransaction);


        return true;
    }

    /**
     * Returns account by it's ID
     *
     * @param accountId : ID of the account
     * @return Account
     */
    @Override
    public Account getAccount(int accountId)
    {
        return this.accountRepository.findById(accountId);
    }

    /**
     * Deletes Account
     *
     * @param account Account
     * @return boolean
     */
    @Override
    public boolean deleteAccount(Account account)
    {
        this.accountRepository.delete(account);

        return true;
    }

    /**
     * Returns true if account with given number exists
     *
     * @param accountNumber : String number of the account
     * @return true if exists, false otherwise
     */
    @Override
    public boolean doesAccountExist(String accountNumber)
    {
        BankCode bankCode = this.bankCodeRepository.findByCode(this.bankCode);
        Account account = this.accountRepository.findByNumberAndBankCode(accountNumber, bankCode);

        return account != null;
    }
}
