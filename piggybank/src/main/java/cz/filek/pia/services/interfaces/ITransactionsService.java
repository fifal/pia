package cz.filek.pia.services.interfaces;

import cz.filek.pia.model.AccountTransaction;
import cz.filek.pia.model.PaymentOrder;
import cz.filek.pia.model.UserAccount;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Interface for transactions service
 *
 * @author Filip Jani
 */
public interface ITransactionsService
{
    /**
     * Method processes all payment orders
     *  - withhold money from user's account
     *  - adds new record to account_transactions table
     *
     * @return boolean : true if operation was successful, false otherwise
     */
    boolean processPaymentOrders();

    /**
     * Returns account balance by account ID
     *
     * @param accountId : ID of the account
     * @return int balance of the account
     */
    Integer getAccountBalance(int accountId);

    /**
     * Returns account usable balance by account ID
     *      usable = sum of transactions - payment orders which hasn't been processed yet
     *
     * @param accountId : ID of the account
     * @return int balance of the account
     */
    Integer getAccountUsableBalance(int accountId);

    /**
     * Saves new Payment order into database
     *
     * @param paymentOrder : PaymentOrder
     * @param userAccount : UserAccount
     * @return true if successful, false otherwise
     */
    boolean saveNewPaymentOrder(PaymentOrder paymentOrder, UserAccount userAccount);

    /**
     * Returns account transaction by transaction ID
     *
     * @param transactionId : ID of the transaction
     * @return AccountTransaction
     */
    AccountTransaction getAccountTransaction(int transactionId);

    /**
     * Returns list of account transactions by account ID limited by Pageable
     *
     * @param userAccount : UserAccount entity
     * @param pageable : Pageable
     * @return List<AccountTransaction>
     */
    List<AccountTransaction> getAccountTransactions(UserAccount userAccount, Pageable pageable);

    /**
     * Returns count of transactions for given account
     *
     * @param userAccount : UserAccount entity
     * @return Integer
     */
    Integer getAccountTransactionsCount(UserAccount userAccount);

    /**
     * Returns list of account transactions by account ID
     *
     * @param accountId : ID of the account
     * @return List<AccountTransaction>
     */
    List<AccountTransaction> getAccountTransactions(int accountId);

    /**
     * Returns count of all transactions
     *
     * @return Integer
     */
    Integer getAccountTransactionsCount();

    /**
     * Returns count of unprocessed payment orders
     *
     * @return Integer
     */
    Integer getUnprocessedPaymentsCount();
}
