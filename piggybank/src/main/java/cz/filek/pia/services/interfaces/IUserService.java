package cz.filek.pia.services.interfaces;

import cz.filek.pia.model.*;

import java.util.List;

/**
 * Interface for user service
 *
 * @author Filip Jani
 */
public interface IUserService
{
    /**
     * Returns List of all users sorted ASC by ID
     *
     * @return List<User>
     */
    List<User> getUsersList();

    /**
     * Generates new card for user with userId for account with accountId
     *
     * @param userId    : ID of user
     * @param accountId : ID of account
     */
    UserCard generateUserCard(int userId, int accountId);

    /**
     * Generates new account for given user
     *
     * @param user : User
     * @return UserAccount : generated users account entity
     */
    UserAccount generateUserAccount(User user);

    /**
     * Adds new User
     *
     * @param userAddress : UserAddress model
     * @return User
     */
    User addNewUser(UserAddress userAddress);

    /**
     * Updates user details: User and UserAddress
     *
     * @param userAddress UserAddress
     * @return UserAddress
     */
    UserAddress updateUser(UserAddress userAddress);

    /**
     * Updates user address info
     *
     * @param user        : User entity
     * @param userAddress : UserAddress entity
     * @return UserAddress
     */
    UserAddress updateUserAddress(User user, UserAddress userAddress);

    /**
     * Deletes user from database
     *
     * @param userId : ID of the user
     * @return true if success, false otherwise
     */
    boolean deleteUser(int userId);

    /**
     * Returns user by his ID
     *
     * @param userId : ID of the user
     * @return User
     */
    User getUser(int userId);

    /**
     * Returns user address by user ID
     *
     * @param userId : ID of the user
     * @return UserAddress
     */
    UserAddress getUserAddress(int userId);

    /**
     * Returns list with user's accounts
     *
     * @param userId : ID of the user
     * @return List<UserAccount>
     */
    List<UserAccount> getUserAccounts(int userId);

    /**
     * Returns list with user's cards
     *
     * @param userId : ID of the user
     * @return List<UserCard>
     */
    List<UserCard> getUserCards(int userId);

    /**
     * Deletes card by id
     *
     * @param cardId : ID of the card
     */
    boolean deleteCard(int cardId);

    /**
     * Returns UserAccount entity by account ID
     *
     * @param accountId : ID of the account
     * @return UserAccount
     */
    UserAccount getUserAccount(int accountId);

    /**
     * Returns User by card ID
     *
     * @param cardId : ID of the card
     * @return User
     */
    User getUserByCardId(int cardId);

    /**
     * Returns User by login
     *
     * @param login : Login of the user
     * @return User
     */
    User getUserByLogin(String login);

    /**
     * Returns User's role list
     *
     * @param user : User entity
     * @return List<UserRole>
     */
    List<UserRole> getUserRole(User user);

    /**
     * Returns list of all user's templates
     *
     * @param user : User entity
     * @return List<UserPaymentTemplate>
     */
    List<UserPaymentTemplate> getUserPaymentTemplates(User user);

    /**
     * Returns count of clients - users with role user
     *
     * @return Integer
     */
    Integer getUsersCount();
}
