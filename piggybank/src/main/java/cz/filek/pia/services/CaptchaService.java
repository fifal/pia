package cz.filek.pia.services;

import cz.filek.pia.backend.GoogleResponse;
import cz.filek.pia.config.CaptchaConfig;
import cz.filek.pia.services.interfaces.ICaptchaService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

/**
 * Service for validating captcha
 *
 * @author Filip Jani
 */
@Service("captchaService")
public class CaptchaService implements ICaptchaService
{
    private CaptchaConfig captchaConfig;
    private RestOperations restTemplate;
    private HttpServletRequest request;

    public CaptchaService(CaptchaConfig captchaConfig,
                          RestOperations restTemplate, HttpServletRequest request)
    {
        this.captchaConfig = captchaConfig;
        this.restTemplate = restTemplate;
        this.request = request;
    }

    /**
     * Returns result of captcha validation
     *
     * @param response : String response
     * @return true if captcha was validated, false otherwise
     */
    @Override
    public boolean processResponse(String response)
    {
        URI verifyUri = URI.create(String.format(
                "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s",
                captchaConfig.getSecret(), response, getClientIP()));

        GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, GoogleResponse.class);
        if(googleResponse != null)
        {
            return googleResponse.isSuccess();
        }

        return false;
    }

    /**
     * Returns IP address of the client
     *
     * @return String
     */
    private String getClientIP()
    {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null)
        {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}
