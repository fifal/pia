package cz.filek.pia.services;

import cz.filek.pia.model.Account;
import cz.filek.pia.model.StandingOrder;
import cz.filek.pia.model.UserAccount;
import cz.filek.pia.model.UserAddress;
import cz.filek.pia.services.interfaces.IMailService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Service for mail sending
 *
 * @author Filip Jani
 */
@Service
public class MailService implements IMailService
{
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.properties.from}")
    private String mailFrom;

    public MailService(@Qualifier("getJavaMailSender") JavaMailSender javaMailSender)
    {
        this.javaMailSender = javaMailSender;
    }

    /**
     * Returns array with header and footer loaded into Strings
     *
     * @return String[]
     * @throws IOException : templates weren't found
     */
    private String[] getHeaderAndFooter() throws IOException
    {
        String header = readTemplateToString("email/header.html");
        String footer = readTemplateToString("email/footer.html");

        return new String[]{header, footer};
    }

    /**
     * Creates MimeMessagePreparator for JavaMailSender
     *
     * @param mailTo  : Email of the client
     * @param subject : Subject of the email
     * @param content : Content of the email
     * @return MimeMessagePreparator
     */
    private MimeMessagePreparator prepareMimeMessage(String mailTo, String subject, String content)
    {

        return mimeMessage -> {
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(mailTo));
            mimeMessage.setFrom(mailFrom);
            mimeMessage.setSubject(subject);

            String[] headerFooter = getHeaderAndFooter();
            String message = headerFooter[0] + content + headerFooter[1];

            mimeMessage.setContent(message, "text/html; charset=UTF-8");
        };
    }

    /**
     * Sends an email to client with his login credentials
     *
     * @param mailTo : Email of the client
     * @param login  : Login number of the client
     * @param pin    : Password of the client
     * @return true if mail was sent successfully, false otherwise
     */
    @Override
    public boolean sendMailAccountCreated(String mailTo, String login, String pin)
    {
        try
        {
            String content = readTemplateToString("email/content/new-account.html");
            content = String.format(content, login, pin);
            MimeMessagePreparator preparator = prepareMimeMessage(mailTo, "Piggy Bank - Váš nový účet", content);
            this.javaMailSender.send(preparator);
        } catch (Exception ex)
        {
            return false;
        }
        return true;
    }

    /**
     * Sends an email to client with information that his account was deleted
     *
     * @param mailTo : Email of the client
     * @param userAccount : UserAccount entity
     * @return true if mail was sent successfully, false otherwise
     */
    @Override
    public boolean sendMailAccountDeleted(String mailTo, UserAccount userAccount)
    {
        try
        {
            String accountNumber = userAccount == null ? "" : userAccount.getAccount().getNumber();
            String bankCode = userAccount == null ? "" : userAccount.getAccount().getBankCode().getCode();

            String content = readTemplateToString("email/content/deleted-account.html");
            content = String.format(content, accountNumber, bankCode);
            MimeMessagePreparator preparator = prepareMimeMessage(mailTo, "Piggy Bank - Zrušení účtu", content);
            this.javaMailSender.send(preparator);

        } catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    /**
     * Sends an email to client with information about changed details of his account by administrator
     *
     * @param mailTo : Email of the client
     * @param userAddress : Array of changes
     * @return true if mail was sent successfully, false otherwise
     */
    @Override
    public boolean sendMailAccountEdited(String mailTo, UserAddress userAddress)
    {
        try
        {
            String content = readTemplateToString("email/content/edited-account.html");
            String changed = "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">Email: </span></div><div class=\"col-9\">" + userAddress.getUser().getEmail() + "</div></div>";
            changed += "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">Login: </span></div><div class=\"col-9\">" + userAddress.getUser().getLogin() + "</div></div>";
            changed += "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">Jméno: </span></div><div class=\"col-9\">" + userAddress.getUser().getFirstName() + "</div></div>";
            changed += "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">Příjmení: </span></div><div class=\"col-9\">" + userAddress.getUser().getLastName() + "</div></div>";
            changed += "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">Telefonní číslo: </span></div><div class=\"col-9\">" + userAddress.getUser().getPhoneNumber() + "</div></div>";
            changed += "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">Město: </span></div><div class=\"col-9\">" + userAddress.getCity() + "</div></div>";
            changed += "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">Ulice: </span></div><div class=\"col-9\">" + userAddress.getStreet() + "</div></div>";
            changed += "<div class=\"row\"><div class=\"col-3\"><span class=\"font-weight-bold\">PSČ: </span></div><div class=\"col-9\">" + userAddress.getZip() + "</div></div>";

            content = String.format(content, changed.toString());

            MimeMessagePreparator preparator = prepareMimeMessage(mailTo, "Piggy Bank - Změna uživatelských údajů", content);
            this.javaMailSender.send(preparator);

        } catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    /**
     * Sends an email to client with information that his standing order couldn't be processed,
     * because he doesn't have enough money on the account.
     *
     * @param mailTo        : Email of the client
     * @param standingOrder : StandingOrder entity
     * @param balance       : User's account balance
     * @return true if mail was sent successfully, false otherwise
     */
    @Override
    public boolean sendMailStandingOrder(String mailTo, StandingOrder standingOrder, Integer balance)
    {
        try
        {
            String orderTitle = standingOrder.getTitle();

            String content = readTemplateToString("email/content/standing-order.html");
            content = String.format(content, orderTitle, standingOrder.getAmount(), balance);
            MimeMessagePreparator preparator = prepareMimeMessage(mailTo, "Piggy Bank - Nedostatek prostředků pro trvalý příkaz", content);
            this.javaMailSender.send(preparator);

        } catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    /**
     * Reads resource into String
     * @param resourcePath : path to the resource
     * @return String
     * @throws IOException exception
     */
    private String readTemplateToString(String resourcePath) throws IOException
    {
        String data;
        ClassPathResource cpr = new ClassPathResource(resourcePath);
        byte[] bdata = FileCopyUtils.copyToByteArray(cpr.getInputStream());
        data = new String(bdata, StandardCharsets.UTF_8);

        return data;
    }
}
