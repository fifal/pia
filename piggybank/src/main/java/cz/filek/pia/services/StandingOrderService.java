package cz.filek.pia.services;

import cz.filek.pia.model.*;
import cz.filek.pia.model.enums.ETransaction;
import cz.filek.pia.repository.*;
import cz.filek.pia.services.interfaces.IMailService;
import cz.filek.pia.services.interfaces.IStandingOrderService;
import cz.filek.pia.services.interfaces.ITransactionsService;
import cz.filek.pia.services.interfaces.IUserService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Service for managing standing orders
 *
 * @author Filip Jani
 */
@Service
@Transactional
public class StandingOrderService implements IStandingOrderService
{
    private StandingOrderRepository standingOrderRepository;
    private UserStandingOrderRepository userStandingOrderRepository;
    private StandingOrderLogRepository standingOrderLogRepository;
    private AccountRepository accountRepository;
    private PaymentOrderRepository paymentOrderRepository;
    private AccountTransactionRepository accountTransactionRepository;
    private ITransactionsService transactionsService;
    private IMailService mailService;
    private IUserService userService;

    public StandingOrderService(StandingOrderRepository standingOrderRepository,
                                UserStandingOrderRepository userStandingOrderRepository,
                                StandingOrderLogRepository standingOrderLogRepository,
                                AccountRepository accountRepository,
                                PaymentOrderRepository paymentOrderRepository,
                                AccountTransactionRepository accountTransactionRepository,
                                TransactionsService transactionsService,
                                MailService mailService,
                                UserService userService)
    {
        this.standingOrderRepository = standingOrderRepository;
        this.userStandingOrderRepository = userStandingOrderRepository;
        this.standingOrderLogRepository = standingOrderLogRepository;
        this.accountRepository = accountRepository;
        this.paymentOrderRepository = paymentOrderRepository;
        this.accountTransactionRepository = accountTransactionRepository;
        this.transactionsService = transactionsService;
        this.mailService = mailService;
        this.userService = userService;
    }

    /**
     * Updates or adds new standing order and assigns it to user
     *
     * @param standingOrder : StandingOrder
     * @param user          : User entity
     * @return UserStandingOrder
     */
    @Override
    public UserStandingOrder updateOrSave(StandingOrder standingOrder, User user, UserAccount userAccount)
    {
        // If StandingOrder exists update
        if (standingOrder.getId() != null)
        {
            UserStandingOrder userStandingOrder = this.userStandingOrderRepository.findByStandingOrder(standingOrder);

            if (userStandingOrder != null)
            {
                standingOrder.setAccount(userAccount.getAccount());
                userStandingOrder.setStandingOrder(standingOrder);

                this.standingOrderRepository.save(standingOrder);

                return this.userStandingOrderRepository.save(userStandingOrder);
            }
        }

        UserStandingOrder newOrder = new UserStandingOrder();
        newOrder.setStandingOrder(standingOrder);
        newOrder.setUser(user);
        standingOrder.setAccount(userAccount.getAccount());

        this.standingOrderRepository.save(standingOrder);
        this.userStandingOrderRepository.save(newOrder);

        return newOrder;
    }

    /**
     * Returns list of all user standing orders
     *
     * @param user : User entity
     * @return List<UserStandingOrder>
     */
    @Override
    public List<UserStandingOrder> getUserStandingOrders(User user)
    {
        return this.userStandingOrderRepository.findAllByUser(user);
    }

    /**
     * Returns standing order by its ID
     *
     * @param id : ID of the standing order
     * @return StandingOrder
     */
    @Override
    public StandingOrder getStandingOrder(int id)
    {
        return this.standingOrderRepository.findById(id);
    }

    /**
     * Deletes standing order by ID
     *
     * @param standingOrderId : ID of the standing order
     * @param user            : User entity
     * @return true if delete was successful, false otherwise
     */
    @Override
    public boolean deleteStandingOrder(int standingOrderId, User user)
    {
        if (!belongsStandingOrderToUser(standingOrderId, user))
        {
            return false;
        }

        StandingOrder standingOrder = this.standingOrderRepository.findById(standingOrderId);
        this.standingOrderRepository.delete(standingOrder);

        return true;
    }

    /**
     * Returns true if standing order with id belongs to user
     *
     * @param standingOrderId : ID of the standing order
     * @param user            : User entity
     * @return true if belongs, false otherwise
     */
    @Override
    public boolean belongsStandingOrderToUser(int standingOrderId, User user)
    {
        UserStandingOrder userStandingOrder = this.userStandingOrderRepository.findByStandingOrderId(standingOrderId);
        if (userStandingOrder == null)
        {
            return false;
        }

        return userStandingOrder.getUser().getId() == user.getId();
    }

    /**
     * Method will process all active standing orders
     *
     * @return true if successful, false otherwise
     */
    @Override
    public boolean processStandingOrders()
    {
        Date date = new Date(System.currentTimeMillis());

        List<StandingOrder> orders = this.standingOrderRepository.findAllByDateAndActive(
                date,
                date,
                true
        );

        for (StandingOrder standingOrder : orders)
        {
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime lastSent = this.standingOrderLogRepository.findLastDateTimeSent(standingOrder);

            if (lastSent == null)
            {
                processStandingOrder(null, standingOrder);
                continue;
            }

            if (ChronoUnit.MINUTES.between(lastSent, now) >= standingOrder.getPeriod())
            {
                StandingOrderLog standingOrderLog = this.standingOrderLogRepository.findBySentDateTimeAndId(lastSent, standingOrder.getId());
                processStandingOrder(standingOrderLog, standingOrder);
            }
        }

        return true;
    }

    /**
     * Switches state of standing order
     * - active <=> not active
     *
     * @param id : ID of the standing order
     * @return boolean
     */
    @Override
    public boolean switchState(int id, User user)
    {
        if(!belongsStandingOrderToUser(id, user)){
            return false;
        }

        StandingOrder standingOrder = this.getStandingOrder(id);
        standingOrder.setActive(!standingOrder.isActive());

        this.standingOrderRepository.save(standingOrder);

        return true;
    }

    /**
     * Processes standing order
     *
     * @param log : StandingOrderLog
     * @param standingOrder : StandingOrder
     */
    private void processStandingOrder(StandingOrderLog log, StandingOrder standingOrder)
    {
        LocalDateTime now = LocalDateTime.now();

        if (log == null)
        {
            log = new StandingOrderLog();
            log.setSentDateTime(now);
            log.setStandingOrder(standingOrder);

        } else
        {
            log.setSentDateTime(now);
        }

        PaymentOrder paymentOrder = new PaymentOrder(standingOrder);
        paymentOrder.setProcessedDate(new Date(System.currentTimeMillis()));

        Account account = this.accountRepository.findById(standingOrder.getAccount().getId());

        // Not enough money to process standing order -> set its state to NOT active
        Integer usableAmount = this.transactionsService.getAccountUsableBalance(account.getId());
        if (usableAmount < standingOrder.getAmount())
        {
            standingOrder.setActive(false);
            this.standingOrderRepository.save(standingOrder);

            User user = this.userService.getUserAccount(account.getId()).getUser();
            this.mailService.sendMailStandingOrder(user.getEmail(), standingOrder, usableAmount);

            return;
        }

        AccountTransaction accountTransaction = new AccountTransaction();
        accountTransaction.setAccount(account);
        accountTransaction.setPaymentOrder(paymentOrder);
        accountTransaction.setTransactionType(ETransaction.bank);
        accountTransaction.setDate(new Date(System.currentTimeMillis()));

        this.paymentOrderRepository.save(paymentOrder);
        this.accountTransactionRepository.save(accountTransaction);
        this.standingOrderLogRepository.save(log);
    }
}
