package cz.filek.pia.backend;

import cz.filek.pia.services.StandingOrderService;
import cz.filek.pia.services.TransactionsService;
import cz.filek.pia.services.interfaces.IStandingOrderService;
import cz.filek.pia.services.interfaces.ITransactionsService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Component used for processing scheduled tasks
 *
 * @author Filip Jani
 */
@Component
public class ScheduledTasks
{
    private ITransactionsService transactionsService;
    private IStandingOrderService standingOrderService;

    public ScheduledTasks(TransactionsService transactionsService,
                          StandingOrderService standingOrderService)
    {
        this.transactionsService = transactionsService;
        this.standingOrderService = standingOrderService;
    }

    /**
     * Method is periodically processing new payment orders into account transactions
     */
    @Scheduled(fixedDelayString = "${piggybank.scheduledtasks.paymentorders}")
    public void processPaymentOrders()
    {
        boolean result = this.transactionsService.processPaymentOrders();
    }

    @Scheduled(fixedDelayString = "${piggybank.secheduledtasks.standingorders}")
    public void processStandingOrders(){boolean result = this.standingOrderService.processStandingOrders(); }
}
