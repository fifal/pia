package cz.filek.pia.frontend;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

/**
 * Provides basic pagination for pages
 *
 *      1. create instance of Paginator
 *      2. prepare Paginator for frontend (prepareForFrontend)
 *      3. add paginator to ModelAndView with name "paginator"
 *      4. set paginator base URL to ModelAndView with name "paginatorUrl"
 *      5. include /tags/customer/paginator.tag in JSP
 */
public class Paginator implements Pageable, Serializable
{
    /**
     * Maximum items per page
     */
    private int limit;
    /**
     * Offset for DB query
     */
    private long offset;
    /**
     * Sort
     */
    private final Sort sort;
    /**
     * Total count of pages
     */
    private int pageCount;
    /**
     * Total count of items
     */
    private int itemCount;
    /**
     * Number of the first page
     */
    private int startingPage;
    /**
     * Number of the last page
     */
    private int endingPage;
    /**
     * Number of current page
     */
    private int currentPage;

    /**
     * Allowed limits of items per page
     */
    private static int[] LIMITS = {10, 25, 50, 100};

    /**
     * Constructor for paginator
     *
     * @param page : int - number of current page
     * @param limit : int - items per page
     * @param sort : Sort
     */
    public Paginator(int page, int limit, Sort sort)
    {
        if (page < 1)
        {
            page = 1;
        }

        this.limit = limit;
        this.currentPage = page;
        this.offset = (page - 1) * limit;
        this.sort = sort;
    }

    /**
     * Returns current page number
     *
     * @return int
     */
    @Override
    public int getPageNumber()
    {
        return currentPage;
    }

    /**
     * Returns items per page count
     *
     * @return int
     */
    @Override
    public int getPageSize()
    {
        return this.limit;
    }

    /**
     * Returns total offset for DB
     *
     * @return int
     */
    @Override
    public long getOffset()
    {
        return this.offset;
    }

    /**
     * Returns Sort
     *
     * @return Sort
     */
    @Override
    public Sort getSort()
    {
        return this.sort;
    }

    /**
     * Returns Paginator with next page
     *
     * @return Pageable
     */
    @Override
    public Pageable next()
    {
        return hasNext() ? new Paginator(getPageNumber() + 1, getPageSize(), getSort()) : this;
    }

    /**
     * Returns Paginator with previous page
     *
     * @return Pageable
     */
    public Paginator previous()
    {
        return hasPrevious() ? new Paginator(getPageNumber() - 1, getPageSize(), getSort()) : this;
    }

    /**
     * Returns Paginator with previous or first page
     *
     * @return Pageable
     */
    @Override
    public Pageable previousOrFirst()
    {
        return previous();
    }

    /**
     * Returns Paginator with the first page
     *
     * @return Pageable
     */
    @Override
    public Pageable first()
    {
        return new Paginator(1, getPageSize(), getSort());
    }

    /**
     * Returns true if paginator has previous page
     *
     * @return true if yes, false otherwise
     */
    @Override
    public boolean hasPrevious()
    {
        return currentPage > startingPage;
    }

    /**
     * Returns true if paginator has nex page
     *
     * @return true if yes, false otherwise
     */
    public boolean hasNext()
    {
        return currentPage < endingPage;
    }

    /**
     * Returns allowed limits
     *
     * @return int[]
     */
    public static int[] getLimits()
    {
        return LIMITS;
    }

    public static int getLimitsLength(){return LIMITS.length;}

    /**
     * Sets some values so we can use them on frontend
     *
     * @param itemCount : total count of items
     */
    public void prepareForFrontend(int itemCount)
    {
        this.itemCount = itemCount;
        this.pageCount = (int) Math.ceil((double) itemCount / (double) this.limit);

        this.startingPage = 1;
        this.endingPage = pageCount;
    }

    /**
     * Returns number of the starting page
     *
     * @return int
     */
    public int getStartingPage()
    {
        return startingPage;
    }

    /**
     * Returns number of the last page
     *
     * @return int
     */
    public int getEndingPage()
    {
        return endingPage;
    }

    /**
     * Returns total count of pages
     *
     * @return int
     */
    public int getPageCount()
    {
        return pageCount;
    }

    /**
     * Returns total count of items
     *
     * @return int
     */
    public int getItemCount()
    {
        return itemCount;
    }
}
