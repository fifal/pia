# Piggy Bank
Semestrální práce k předmětu KIV/PIA.

## Potřebný SW
- docker
- docker-compose

## Spuštění
Ve složce `docker/`:

- Zbuildování kontajneru: `docker build -t pia/fjani .`
- Spuštění: `docker-compose up`

Aplikace je přístupná na http://localhost:8080 a MaliHog pro kontrolu emailů na http://localhost:8025.