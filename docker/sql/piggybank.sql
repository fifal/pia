--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3 (Debian 10.3-2)
-- Dumped by pg_dump version 10.3 (Debian 10.3-2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE pia; Type: COMMENT; Schema: -; Owner: fifal
--

COMMENT ON DATABASE pia IS 'Databáze k předmětu KIV/PIA - elektronické bankovnictví';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: e_limit; Type: TYPE; Schema: public; Owner: fifal
--

CREATE TYPE public.e_limit AS ENUM (
    'internet',
    'withdraw',
    'pay'
);


ALTER TYPE public.e_limit OWNER TO fifal;

--
-- Name: e_role; Type: TYPE; Schema: public; Owner: fifal
--

CREATE TYPE public.e_role AS ENUM (
    'admin',
    'user'
);


ALTER TYPE public.e_role OWNER TO fifal;

--
-- Name: TYPE e_role; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TYPE public.e_role IS 'Enum s možnými rolemi uživatele';


--
-- Name: e_transaction; Type: TYPE; Schema: public; Owner: fifal
--

CREATE TYPE public.e_transaction AS ENUM (
    'card',
    'atm',
    'bank'
);


ALTER TYPE public.e_transaction OWNER TO fifal;

--
-- Name: TYPE e_transaction; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TYPE public.e_transaction IS 'Enum s typy plateb';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.account (
    id integer NOT NULL,
    bank_code character varying(4) NOT NULL,
    number character varying(10) NOT NULL,
    prefix character varying(4) DEFAULT NULL::character varying
);


ALTER TABLE public.account OWNER TO fifal;

--
-- Name: TABLE account; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.account IS 'Základní informace o účtu';


--
-- Name: COLUMN account.bank_code; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.account.bank_code IS 'Cizí klíč, bankovní kód';


--
-- Name: COLUMN account.prefix; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.account.prefix IS 'Předčíslí účtu';


--
-- Name: account_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_id_seq OWNER TO fifal;

--
-- Name: account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.account_id_seq OWNED BY public.account.id;


--
-- Name: account_transactions; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.account_transactions (
    id integer NOT NULL,
    account_id integer NOT NULL,
    type public.e_transaction NOT NULL,
    payment_order_id integer,
    date date DEFAULT now() NOT NULL
);


ALTER TABLE public.account_transactions OWNER TO fifal;

--
-- Name: TABLE account_transactions; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.account_transactions IS 'Všechny transakce provedené na účtu';


--
-- Name: COLUMN account_transactions.type; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.account_transactions.type IS 'Typ transakce, e_transaction';


--
-- Name: account_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.account_transactions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_transactions_id_seq OWNER TO fifal;

--
-- Name: account_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.account_transactions_id_seq OWNED BY public.account_transactions.id;


--
-- Name: bank_codes; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.bank_codes (
    code character varying(4) NOT NULL,
    name character varying(255),
    is_active boolean DEFAULT true NOT NULL
);


ALTER TABLE public.bank_codes OWNER TO fifal;

--
-- Name: TABLE bank_codes; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.bank_codes IS 'Číselník s kódy jednotlivých bank';


--
-- Name: card; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.card (
    id integer NOT NULL,
    number character varying(16) NOT NULL,
    valid_from date NOT NULL,
    valid_to date NOT NULL,
    cvv character varying(3) NOT NULL,
    is_blocked boolean DEFAULT false NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE public.card OWNER TO fifal;

--
-- Name: TABLE card; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.card IS 'Platební karty';


--
-- Name: COLUMN card.number; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.card.number IS 'Číslo platební karty - 16 čísel bez mezer';


--
-- Name: COLUMN card.valid_from; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.card.valid_from IS 'Platnost od';


--
-- Name: COLUMN card.valid_to; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.card.valid_to IS 'Platnost do';


--
-- Name: COLUMN card.cvv; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.card.cvv IS 'CVV kód';


--
-- Name: COLUMN card.is_blocked; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.card.is_blocked IS 'Udává zda je karta blokována';


--
-- Name: card_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.card_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.card_id_seq OWNER TO fifal;

--
-- Name: card_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.card_id_seq OWNED BY public.card.id;


--
-- Name: card_limits; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.card_limits (
    id integer NOT NULL,
    card_id integer NOT NULL,
    "limit" public.e_limit NOT NULL
);


ALTER TABLE public.card_limits OWNER TO fifal;

--
-- Name: card_limits_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.card_limits_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.card_limits_id_seq OWNER TO fifal;

--
-- Name: card_limits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.card_limits_id_seq OWNED BY public.card_limits.id;


--
-- Name: payment_order; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.payment_order (
    id integer NOT NULL,
    account_id integer NOT NULL,
    message character varying(255),
    const_symbol character varying(10),
    var_symbol character varying(10),
    spec_symbol character varying(10),
    due_date date NOT NULL,
    benef_account character varying(15) NOT NULL,
    benef_bank_code character varying(4) NOT NULL,
    amount integer NOT NULL,
    processed_date date
);


ALTER TABLE public.payment_order OWNER TO fifal;

--
-- Name: payment_order_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.payment_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_order_id_seq OWNER TO fifal;

--
-- Name: payment_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.payment_order_id_seq OWNED BY public.payment_order.id;


--
-- Name: payment_order_template_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.payment_order_template_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_order_template_seq OWNER TO fifal;

--
-- Name: payment_order_template; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.payment_order_template (
    id integer DEFAULT nextval('public.payment_order_template_seq'::regclass) NOT NULL,
    account_id integer NOT NULL,
    message character varying(255),
    const_symbol character varying(10),
    var_symbol character varying(10),
    spec_symbol character varying(10),
    benef_account character varying(15) NOT NULL,
    benef_bank_code character varying(4) NOT NULL,
    amount integer NOT NULL,
    title character varying(255)
);


ALTER TABLE public.payment_order_template OWNER TO fifal;

--
-- Name: COLUMN payment_order_template.title; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public.payment_order_template.title IS 'Název šablony';


--
-- Name: role; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.role (
    id integer NOT NULL,
    role public.e_role NOT NULL
);


ALTER TABLE public.role OWNER TO fifal;

--
-- Name: TABLE role; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.role IS 'Role uživatelů';


--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO fifal;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: standing_order; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.standing_order (
    id integer NOT NULL,
    account_id integer NOT NULL,
    message character varying(255),
    const_symbol character varying(10),
    var_symbol character varying(10),
    spec_symbol character varying(10),
    benef_account character varying(15) NOT NULL,
    benef_bank_code character varying(4) NOT NULL,
    amount integer NOT NULL,
    standing_start_date date NOT NULL,
    standing_stop_date date NOT NULL,
    title character varying(255),
    period integer NOT NULL,
    active boolean NOT NULL
);


ALTER TABLE public.standing_order OWNER TO fifal;

--
-- Name: standing_order_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.standing_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.standing_order_id_seq OWNER TO fifal;

--
-- Name: standing_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.standing_order_id_seq OWNED BY public.standing_order.id;


--
-- Name: standing_order_log; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.standing_order_log (
    id integer NOT NULL,
    standing_order_id integer NOT NULL,
    standing_order_sent_datetime timestamp without time zone NOT NULL
);


ALTER TABLE public.standing_order_log OWNER TO fifal;

--
-- Name: standing_order_log_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.standing_order_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.standing_order_log_id_seq OWNER TO fifal;

--
-- Name: standing_order_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.standing_order_log_id_seq OWNED BY public.standing_order_log.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    password character varying(255) NOT NULL,
    login character varying(32) NOT NULL,
    email character varying(100) NOT NULL,
    firstname character varying(30) NOT NULL,
    lastname character varying(60) NOT NULL,
    phone_number character varying(9),
    pin character varying(11) DEFAULT (123456 / 1234) NOT NULL
);


ALTER TABLE public."user" OWNER TO fifal;

--
-- Name: TABLE "user"; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public."user" IS 'Ukládání základních informací o uživateli';


--
-- Name: COLUMN "user".password; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public."user".password IS 'Heslo';


--
-- Name: COLUMN "user".pin; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON COLUMN public."user".pin IS 'Rodné číslo zákazníka';


--
-- Name: user_accounts; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.user_accounts (
    id integer NOT NULL,
    user_id integer NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE public.user_accounts OWNER TO fifal;

--
-- Name: TABLE user_accounts; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.user_accounts IS 'Přiřazení účtů k uživateli';


--
-- Name: user_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.user_accounts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_accounts_id_seq OWNER TO fifal;

--
-- Name: user_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.user_accounts_id_seq OWNED BY public.user_accounts.id;


--
-- Name: user_address; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.user_address (
    id integer NOT NULL,
    user_id integer NOT NULL,
    city character varying(50),
    street character varying(100),
    zip character varying(5)
);


ALTER TABLE public.user_address OWNER TO fifal;

--
-- Name: TABLE user_address; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.user_address IS 'Ukládání adresy zákazníka';


--
-- Name: user_address_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.user_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_address_id_seq OWNER TO fifal;

--
-- Name: user_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.user_address_id_seq OWNED BY public.user_address.id;


--
-- Name: user_cards; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.user_cards (
    id integer NOT NULL,
    user_id integer NOT NULL,
    card_id integer NOT NULL
);


ALTER TABLE public.user_cards OWNER TO fifal;

--
-- Name: TABLE user_cards; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.user_cards IS 'Platební karty uživatele';


--
-- Name: user_cards_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.user_cards_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_cards_id_seq OWNER TO fifal;

--
-- Name: user_cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.user_cards_id_seq OWNED BY public.user_cards.id;


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO fifal;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: user_payment_templates; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.user_payment_templates (
    user_id integer NOT NULL,
    payment_order_template_id integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.user_payment_templates OWNER TO fifal;

--
-- Name: user_payment_templates_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.user_payment_templates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_payment_templates_id_seq OWNER TO fifal;

--
-- Name: user_payment_templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.user_payment_templates_id_seq OWNED BY public.user_payment_templates.id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.user_roles (
    id integer NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.user_roles OWNER TO fifal;

--
-- Name: TABLE user_roles; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON TABLE public.user_roles IS 'Tabulka sloužící k přiřazování rolí uživatelům';


--
-- Name: user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.user_roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_roles_id_seq OWNER TO fifal;

--
-- Name: user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.user_roles_id_seq OWNED BY public.user_roles.id;


--
-- Name: user_standing_orders; Type: TABLE; Schema: public; Owner: fifal
--

CREATE TABLE public.user_standing_orders (
    user_id integer NOT NULL,
    standing_order_id integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.user_standing_orders OWNER TO fifal;

--
-- Name: user_standing_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: fifal
--

CREATE SEQUENCE public.user_standing_orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_standing_orders_id_seq OWNER TO fifal;

--
-- Name: user_standing_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fifal
--

ALTER SEQUENCE public.user_standing_orders_id_seq OWNED BY public.user_standing_orders.id;


--
-- Name: account id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account ALTER COLUMN id SET DEFAULT nextval('public.account_id_seq'::regclass);


--
-- Name: account_transactions id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account_transactions ALTER COLUMN id SET DEFAULT nextval('public.account_transactions_id_seq'::regclass);


--
-- Name: card id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.card ALTER COLUMN id SET DEFAULT nextval('public.card_id_seq'::regclass);


--
-- Name: card_limits id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.card_limits ALTER COLUMN id SET DEFAULT nextval('public.card_limits_id_seq'::regclass);


--
-- Name: payment_order id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.payment_order ALTER COLUMN id SET DEFAULT nextval('public.payment_order_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: standing_order id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.standing_order ALTER COLUMN id SET DEFAULT nextval('public.standing_order_id_seq'::regclass);


--
-- Name: standing_order_log id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.standing_order_log ALTER COLUMN id SET DEFAULT nextval('public.standing_order_log_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Name: user_accounts id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_accounts ALTER COLUMN id SET DEFAULT nextval('public.user_accounts_id_seq'::regclass);


--
-- Name: user_address id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_address ALTER COLUMN id SET DEFAULT nextval('public.user_address_id_seq'::regclass);


--
-- Name: user_cards id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_cards ALTER COLUMN id SET DEFAULT nextval('public.user_cards_id_seq'::regclass);


--
-- Name: user_payment_templates id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_payment_templates ALTER COLUMN id SET DEFAULT nextval('public.user_payment_templates_id_seq'::regclass);


--
-- Name: user_roles id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_roles ALTER COLUMN id SET DEFAULT nextval('public.user_roles_id_seq'::regclass);


--
-- Name: user_standing_orders id; Type: DEFAULT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_standing_orders ALTER COLUMN id SET DEFAULT nextval('public.user_standing_orders_id_seq'::regclass);


--
-- Name: 27740; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('27740');


ALTER LARGE OBJECT 27740 OWNER TO postgres;

--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.account (id, bank_code, number, prefix) FROM stdin;
50	4488	6329388614	\N
51	4488	6965412685	\N
\.


--
-- Data for Name: account_transactions; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.account_transactions (id, account_id, type, payment_order_id, date) FROM stdin;
200	50	bank	147	2019-01-06
201	51	bank	148	2019-01-06
202	50	bank	149	2019-01-06
203	51	bank	149	2019-01-06
204	51	bank	150	2019-01-06
205	50	bank	150	2019-01-06
\.


--
-- Data for Name: bank_codes; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.bank_codes (code, name, is_active) FROM stdin;
4488	Piggy Bank a.s.	t
8296	PaySysEU s.r.o.	t
8297	EUPSProvider s.r.o.	t
8298	Andraste Capital s.r.o.	t
2070	Moravský Peněžní Ústav – spořitelní družstvo	f
2100	Hypoteční banka, a.s.	f
2200	Peněžní dům, spořitelní družstvo	f
2220	Artesa, spořitelní družstvo	f
6210	mBank S.A., organizační složka	f
2240	Poštová banka, a.s., pobočka Česká republika	f
6300	BNP Paribas S.A., pobočka Česká republika	f
2250	Banka CREDITAS a.s.	f
2260	NEY spořitelní družstvo	f
2275	Podnikatelská družstevní záložna	f
2600	Citibank Europe plc, organizační složka	f
2700	UniCredit Bank Czech Republic and Slovakia, a.s.	f
3030	Air Bank a.s.	f
3050	BNP Paribas Personal Finance SA, odštěpný závod	f
3060	PKO BP S.A., Czech Branch	f
3500	ING Bank N.V.	f
4000	Expobank CZ a.s.	f
4300	Českomoravská záruční a rozvojová banka, a.s.	f
5500	Raiffeisenbank a.s.	f
5800	J & T BANKA, a.s.	f
6000	PPF banka a.s.	f
6100	Equa bank a.s.	f
6200	COMMERZBANK Aktiengesellschaft, pobočka Praha	f
6700	Všeobecná úverová banka a.s., pobočka Praha	f
6800	Sberbank CZ, a.s.	f
7910	Deutsche Bank Aktiengesellschaft Filiale Prag, organizační složka	f
7940	Waldviertler Sparkasse Bank AG	f
7950	Raiffeisen stavební spořitelna a.s.	f
7960	Českomoravská stavební spořitelna, a.s.	f
7970	Wüstenrot - stavební spořitelna a.s.	f
7980	Wüstenrot hypoteční banka a.s.	f
7990	Modrá pyramida stavební spořitelna, a.s.	f
8030	Volksbank Raiffeisenbank Nordoberpfalz eG pobočka Cheb	f
8040	Oberbank AG pobočka Česká republika	f
8060	Stavební spořitelna České spořitelny, a.s.	f
8090	Česká exportní banka, a.s.	f
8150	HSBC Bank plc - pobočka Praha	f
8200	PRIVAT BANK AG der Raiffeisenlandesbank Oberösterreich Aktiengesellschaft, pobočka Česká republika	f
8215	ALTERNATIVE PAYMENT SOLUTIONS, s.r.o.	f
8220	Payment Execution s.r.o.	f
8225	ORANGETRUST s.r.o.	f
8230	EEPAYS s.r.o.	f
8240	Družstevní záložna Kredit	f
8250	Bank of China (Hungary) Close Ltd. Prague branch, odštěpný závod	f
8260	PAYMASTER a.s.	f
8265	Industrial and Commercial Bank of China Limited Prague Branch, odštěpný závod	f
8270	Fairplay Pay s.r.o.	f
8280	B-Efekt a.s.	f
8290	EUROPAY s.r.o.	f
8291	Business Credit s.r.o.	f
8292	Money Change s.r.o.	f
8293	Mercurius partners s.r.o.	f
8294	GrisPayUnion s.r.o.	f
8295	NOVARED s.r.o.	f
0100	Komerční banka, a.s.	t
0300	Československá obchodní banka, a.s.	t
0600	MONETA Money Bank, a.s.	t
0710	Česká národní banka	t
0800	Česká spořitelna, a.s.	t
1478	Testovací banka	t
2010	Fio banka, a.s.	t
2020	MUFG Bank (Europe) N.V. Prague Branch	t
2030	Československé úvěrní družstvo	t
2060	Citfin, spořitelní družstvo	t
\.


--
-- Data for Name: card; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.card (id, number, valid_from, valid_to, cvv, is_blocked, account_id) FROM stdin;
37	7051165802953330	2019-01-06	2021-01-06	368	f	50
38	3439082617349750	2019-01-06	2021-01-06	997	f	51
\.


--
-- Data for Name: card_limits; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.card_limits (id, card_id, "limit") FROM stdin;
\.


--
-- Data for Name: payment_order; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.payment_order (id, account_id, message, const_symbol, var_symbol, spec_symbol, due_date, benef_account, benef_bank_code, amount, processed_date) FROM stdin;
147	50	Manuální dobití účtu z administrace.	MNLPGY	MNLPGY	MNLPGY	2019-01-06	6329388614	4488	100000	2019-01-06
148	51	Manuální dobití účtu z administrace.	MNLPGY	MNLPGY	MNLPGY	2019-01-06	6965412685	4488	50000	2019-01-06
149	50	Platba od User0001				2019-01-06	6965412685	4488	-399	2019-01-06
150	51	Platba od User0002				2019-01-06	6329388614	4488	-999	2019-01-06
\.


--
-- Data for Name: payment_order_template; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.payment_order_template (id, account_id, message, const_symbol, var_symbol, spec_symbol, benef_account, benef_bank_code, amount, title) FROM stdin;
12	50	Platba od User0001				6965412685	4488	399	Šablona User0002
13	51	Platba od User0002				6329388614	4488	999	Šablona User0001
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.role (id, role) FROM stdin;
1	admin
2	user
\.


--
-- Data for Name: standing_order; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.standing_order (id, account_id, message, const_symbol, var_symbol, spec_symbol, benef_account, benef_bank_code, amount, standing_start_date, standing_stop_date, title, period, active) FROM stdin;
\.


--
-- Data for Name: standing_order_log; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.standing_order_log (id, standing_order_id, standing_order_sent_datetime) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public."user" (id, password, login, email, firstname, lastname, phone_number, pin) FROM stdin;
21	$2a$10$LkxqpaN9FT8b920GvARaIebXRd5EwMfWpy3IHn9MnJ7jwwf3VQvwO	Admin001	admin001@piggybank.cz	Admin	001	\N	100
23	$2a$10$UP.df4ovEcAam7YBnEzbWe9Op8ixAQ0V1eLb6hoSGdo.RO2WqPKbO	User0001	user0001@piggybank.cz	User	0001	123456799	112233/1234
24	$2a$10$mIAAI2XUgT5H9AMFHZSXhOLFl1DGSAu0VZvNOcdCmuia9nrqTV2xu	User0002	user0002@piggybank.cz	User	0002	123456788	223344/1234
\.


--
-- Data for Name: user_accounts; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.user_accounts (id, user_id, account_id) FROM stdin;
51	23	50
52	24	51
\.


--
-- Data for Name: user_address; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.user_address (id, user_id, city, street, zip) FROM stdin;
13	23	Plzeň	Máchova 2434/20	30100
16	24	Plzeň	Ulice	30100
\.


--
-- Data for Name: user_cards; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.user_cards (id, user_id, card_id) FROM stdin;
33	23	37
34	24	38
\.


--
-- Data for Name: user_payment_templates; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.user_payment_templates (user_id, payment_order_template_id, id) FROM stdin;
23	12	9
24	13	10
\.


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.user_roles (id, user_id, role_id) FROM stdin;
8	21	1
9	23	2
10	24	2
\.


--
-- Data for Name: user_standing_orders; Type: TABLE DATA; Schema: public; Owner: fifal
--

COPY public.user_standing_orders (user_id, standing_order_id, id) FROM stdin;
\.


--
-- Name: account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.account_id_seq', 51, true);


--
-- Name: account_transactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.account_transactions_id_seq', 205, true);


--
-- Name: card_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.card_id_seq', 38, true);


--
-- Name: card_limits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.card_limits_id_seq', 3, true);


--
-- Name: payment_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.payment_order_id_seq', 150, true);


--
-- Name: payment_order_template_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.payment_order_template_seq', 13, true);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.role_id_seq', 1, false);


--
-- Name: standing_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.standing_order_id_seq', 15, true);


--
-- Name: standing_order_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.standing_order_log_id_seq', 2, true);


--
-- Name: user_accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.user_accounts_id_seq', 52, true);


--
-- Name: user_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.user_address_id_seq', 32, true);


--
-- Name: user_cards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.user_cards_id_seq', 34, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.user_id_seq', 46, true);


--
-- Name: user_payment_templates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.user_payment_templates_id_seq', 10, true);


--
-- Name: user_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.user_roles_id_seq', 22, true);


--
-- Name: user_standing_orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fifal
--

SELECT pg_catalog.setval('public.user_standing_orders_id_seq', 9, true);


--
-- Data for Name: BLOBS; Type: BLOBS; Schema: -; Owner: 
--

BEGIN;

SELECT pg_catalog.lo_open('27740', 131072);
SELECT pg_catalog.lo_close(0);

COMMIT;

--
-- Name: account account_number_key; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_number_key UNIQUE (number);


--
-- Name: account account_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (id);


--
-- Name: account_transactions account_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account_transactions
    ADD CONSTRAINT account_transactions_pkey PRIMARY KEY (id);


--
-- Name: bank_codes bank_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.bank_codes
    ADD CONSTRAINT bank_codes_pkey PRIMARY KEY (code);


--
-- Name: card_limits card_limits_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.card_limits
    ADD CONSTRAINT card_limits_pkey PRIMARY KEY (id);


--
-- Name: card card_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.card
    ADD CONSTRAINT card_pkey PRIMARY KEY (id);


--
-- Name: payment_order payment_order_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.payment_order
    ADD CONSTRAINT payment_order_pkey PRIMARY KEY (id);


--
-- Name: payment_order_template payment_order_template_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.payment_order_template
    ADD CONSTRAINT payment_order_template_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: standing_order_log standing_order_log_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.standing_order_log
    ADD CONSTRAINT standing_order_log_pkey PRIMARY KEY (id);


--
-- Name: standing_order standing_order_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.standing_order
    ADD CONSTRAINT standing_order_pkey PRIMARY KEY (id);


--
-- Name: user_accounts uq_account_id; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT uq_account_id UNIQUE (account_id);


--
-- Name: user_cards uq_card_id; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_cards
    ADD CONSTRAINT uq_card_id UNIQUE (card_id);


--
-- Name: user uq_email; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT uq_email UNIQUE (email);


--
-- Name: user uq_login; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT uq_login UNIQUE (login);


--
-- Name: user uq_pin; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT uq_pin UNIQUE (pin);


--
-- Name: user_address uq_user_Id; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_address
    ADD CONSTRAINT "uq_user_Id" UNIQUE (user_id);


--
-- Name: user_accounts user_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT user_accounts_pkey PRIMARY KEY (id);


--
-- Name: user_address user_address_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_address
    ADD CONSTRAINT user_address_pkey PRIMARY KEY (id);


--
-- Name: user_cards user_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_cards
    ADD CONSTRAINT user_cards_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_roles user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);


--
-- Name: user_standing_orders user_standing_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_standing_orders
    ADD CONSTRAINT user_standing_orders_pkey PRIMARY KEY (id);


--
-- Name: account_transactions fk_account_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account_transactions
    ADD CONSTRAINT fk_account_id FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- Name: CONSTRAINT fk_account_id ON account_transactions; Type: COMMENT; Schema: public; Owner: fifal
--

COMMENT ON CONSTRAINT fk_account_id ON public.account_transactions IS 'Cizí klíč account.id';


--
-- Name: payment_order fk_account_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.payment_order
    ADD CONSTRAINT fk_account_id FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- Name: user_accounts fk_account_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT fk_account_id FOREIGN KEY (account_id) REFERENCES public.account(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: payment_order_template fk_account_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.payment_order_template
    ADD CONSTRAINT fk_account_id FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- Name: standing_order fk_account_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.standing_order
    ADD CONSTRAINT fk_account_id FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- Name: account fk_bank_code; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT fk_bank_code FOREIGN KEY (bank_code) REFERENCES public.bank_codes(code);


--
-- Name: payment_order fk_bank_code; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.payment_order
    ADD CONSTRAINT fk_bank_code FOREIGN KEY (benef_bank_code) REFERENCES public.bank_codes(code);


--
-- Name: payment_order_template fk_bank_code; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.payment_order_template
    ADD CONSTRAINT fk_bank_code FOREIGN KEY (benef_bank_code) REFERENCES public.bank_codes(code);


--
-- Name: standing_order fk_bank_code; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.standing_order
    ADD CONSTRAINT fk_bank_code FOREIGN KEY (benef_bank_code) REFERENCES public.bank_codes(code);


--
-- Name: user_cards fk_card_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_cards
    ADD CONSTRAINT fk_card_id FOREIGN KEY (card_id) REFERENCES public.card(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: card_limits fk_card_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.card_limits
    ADD CONSTRAINT fk_card_id FOREIGN KEY (card_id) REFERENCES public.card(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: card fk_card_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.card
    ADD CONSTRAINT fk_card_id FOREIGN KEY (account_id) REFERENCES public.account(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: account_transactions fk_payment_order_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.account_transactions
    ADD CONSTRAINT fk_payment_order_id FOREIGN KEY (payment_order_id) REFERENCES public.payment_order(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_payment_templates fk_payment_order_template; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_payment_templates
    ADD CONSTRAINT fk_payment_order_template FOREIGN KEY (payment_order_template_id) REFERENCES public.payment_order_template(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_roles fk_role_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES public.role(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_standing_orders fk_standing_order_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_standing_orders
    ADD CONSTRAINT fk_standing_order_id FOREIGN KEY (standing_order_id) REFERENCES public.standing_order(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: standing_order_log fk_standing_order_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.standing_order_log
    ADD CONSTRAINT fk_standing_order_id FOREIGN KEY (standing_order_id) REFERENCES public.standing_order(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_address fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_address
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_accounts fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_cards fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_cards
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_roles fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_payment_templates fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_payment_templates
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_standing_orders fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: fifal
--

ALTER TABLE ONLY public.user_standing_orders
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

