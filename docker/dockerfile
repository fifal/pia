FROM debian:buster

MAINTAINER Filip Jani <fjani@students.zcu.cz>

# Nastavení Debianu
ENV DEBIAN_FRONTEND=noninteractive

# Ošetření chyby: invoke-rc.d: policy-rc.d denied execution of start.
RUN sed -i "s/^exit 101$/exit 0/" /usr/sbin/policy-rc.d

# Základní repozitář pro Debian Jessie
RUN echo "deb http://ftp.cz.debian.org/debian buster main" > /etc/apt/sources.list.d/jessie.list

# Aktualizace definice balíčků a instalace základních balíčků
RUN apt-get update && \
    apt-get install -y --no-install-recommends wget apt-transport-https ca-certificates

RUN apt-get install openjdk-8-jdk-headless openjdk-8-jre-headless ca-certificates-java maven -y

RUN apt-get install -y postgresql postgresql-client postgresql-contrib  

# Pročištění souborů a cache dockerimage
RUN apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /var/lib/log/* /tmp/* /var/tmp/*

RUN useradd fifal -m

# Konfigurace Postgresql
USER postgres
COPY sql/piggybank.sql /home/postgres/

# Sleep 2 aby bylo jistý, že DB už běží
RUN /etc/init.d/postgresql start && sleep 2 &&\
    psql --command "CREATE USER fifal WITH SUPERUSER PASSWORD 'docker';" &&\
    createdb -O fifal pia &&\
    psql -d pia -f /home/postgres/piggybank.sql  

# Povolení připojení z venku
RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/11/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/11/main/postgresql.conf

CMD ["/usr/lib/postgresql/11/bin/postgres", "-D", "/var/lib/postgresql/11/main", "-c", "config_file=/etc/postgresql/11/main/postgresql.conf"]

USER root

# Instalace Mailhog
RUN apt-get update && apt-get install -y git
RUN wget https://storage.googleapis.com/golang/go1.9.linux-amd64.tar.gz
RUN tar -C /etc -xzf go1.9.linux-amd64.tar.gz go && rm go1.9.linux-amd64.tar.gz
RUN mkdir -p /root/gocode
ENV GOPATH=/root/gocode
RUN /etc/go/bin/go get github.com/mailhog/MailHog
RUN mv /root/gocode/bin/MailHog /usr/local/bin
RUN rm -rf /root/gocode

COPY bash/start.sh /home/fifal/
CMD /home/fifal/start.sh
